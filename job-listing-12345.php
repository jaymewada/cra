<?php include "components/header.php" ?>
<section class="inner-banner bg-secondary jumbotron job-desc-breadcrumb">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mb-5">
                                <li class="breadcrumb-item"><a href="./">Home</a></li>
                                <li class="breadcrumb-item" aria-current="page">CAREERS</li>
                                <li class="breadcrumb-item" aria-current="page">JOB OPENINGS</li>
                                <li class="breadcrumb-item active" aria-current="page">Business Development</li>
                            </ol>
                        </nav>
                        <h1 class="heading-1 text-white mb-5">Business Development</h1>
                        <p class="heading-3 text-white font-regular mt-3 pt-3"><span><img class="job-dec-icon"
                                    src="https://www.careratingsafrica.com/public/frontend-assets/images/bag.svg"
                                    alt=""></span> 12345</p>
                        <p class="heading-3 text-white font-regular mt-3 pt-3"><span><img class="job-dec-icon"
                                    src="https://www.careratingsafrica.com/public/frontend-assets/images/location-job.svg"
                                    alt=""></span> Pune</p>
                        <p class="heading-3 text-white font-regular mt-3 pt-3"><span><img class="job-dec-icon"
                                    src="https://www.careratingsafrica.com/public/frontend-assets/images/time.svg"
                                    alt=""></span> Posted 4 months ago</p>
                        <a href="#apply_now_career" class="btn btn-primary mt-5 apply-btn">APPLY NOW</a>
                    </div>
                    <div class="col-md-4">
                        <img class="job-desc-img"
                            src="https://www.careratingsafrica.com/public/frontend-assets/images/analyst.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="join-our-panel-sec bg-img overflow-hidden job-desc1" id="apply_now_career">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-12">
                <div class="row gx-5">
                    <div class="col-lg-6 col-md-12">
                        <h2 class="heading-1 text-white mt-5 pt-4 mb-3">Apply Now</h2>
                        <h3 class="heading-2 text-white mt-5">Apply for your desired position</h3>

                    </div>
                    <div class="col-lg-6  col-md-12">
                        <div class="contact-form bg-secondary ">
                            <form class="row form-style-1" id="form_apply_now" name="form_apply_now" action="#"
                                method="post" enctype="multipart/form-data" novalidate="">
                                <div class="row">
                                    <div class="alert alert-success print-applynow-success-msg" style="display:none">
                                        <ul></ul>
                                    </div>
                                    <div class="alert alert-danger print-applynow-error-msg" style="display:none">
                                        <ul></ul>
                                    </div>
                                </div>
                                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                                    <h4 class="heading-2 text-white">Apply Now</h4>
                                </div>
                                <div class="col-md-12 gx-5">
                                    <div class="form-floating">
                                        <input type="text" value="sulabha naware" class="form-control" name="full_name"
                                            id="full_name" placeholder="name@example.com" required=""
                                            data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Full Name" jf-ext-cache-id="7">
                                        <label for="floatingInput">Full Name</label>
                                    </div>
                                </div>
                                <div class="col-md-6 gx-5">
                                    <div class="form-floating">
                                        <input type="email" value="sulabha.naware@evolutionco.com" class="form-control"
                                            name="email" id="email" placeholder="name@example.com" required=""
                                            data-parsley-trigger="focusout" data-parsley-required-message="Enter Email"
                                            jf-ext-cache-id="8">
                                        <label for="floatingInput">Email</label>
                                    </div>
                                </div>
                                <div class="col-md-6 gx-5">
                                    <div class="form-floating">
                                        <input type="text" value="9881262200" class="form-control"
                                            data-parsley-pattern="^([9]{1})([234789]{1})([0-9]{8})$" name="mobile"
                                            id="mobile" placeholder="1234567890" required=""
                                            data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Mobile Number"
                                            data-parsley-pattern-message="Enter Valid Mobile Number"
                                            jf-ext-cache-id="9">
                                        <label for="floatingInput">Mobile Number</label>
                                    </div>
                                </div>
                                <div class="col-md-12 gx-5">
                                    <div class="form-floating">
                                        <input type="text" value="101, DP road" class="form-control parsley-success"
                                            name="address" id="address" placeholder="Address" maxlength="300"
                                            required="" data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Address" jf-ext-cache-id="10"
                                            data-parsley-id="31">
                                        <label for="floatingInput">Address</label>
                                        <div class="msz-count-limit">
                                            <span id="current">0</span>
                                            <span id="maximum">/ 150</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 gx-5">
                                    <div class="form-floating ">
                                        <input type="text" value="AXWPN7031R"
                                            class="form-control text-uppercase parsley-success"
                                            data-parsley-pattern="[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}" name="pan_number"
                                            id="pan_number" placeholder="XXXXXXXXXX" required=""
                                            data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Pan Number"
                                            data-parsley-pattern-message="Enter Valid Pan Number" jf-ext-cache-id="11"
                                            data-parsley-id="33">
                                        <label for="floatingInput">PAN Number</label>
                                    </div>
                                </div>
                                <input type="hidden" name="position" id="position" value="Business Development">
                                <div class="col-md-6 gx-5">
                                    <div class="form-floating ">
                                        <input type="text" value="From Online portal" class="form-control"
                                            name="message" id="message" placeholder="How did you hear about the job?"
                                            required="" data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Message" jf-ext-cache-id="12">
                                        <label for="floatingInput">How did you hear about the job?</label>
                                    </div>
                                </div>
                                <div class="col-md-5 gx-5">
                                    <div class="image-upload-wrap">
                                        <input class="file-upload-input" name="resume" onchange="readURL(this);"
                                            type="file"
                                            accept=".pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document">
                                        <div class="drag-text">
                                            <h3 class="form-control font-regular text-white">Drop your resume here <img
                                                    src="https://www.careratingsafrica.com/public/frontend-assets/images/file.png"
                                                    class="img-fluid" width="33" height="33" alt=""></h3>
                                        </div>
                                    </div>
                                    <div class="file-upload-content">
                                        <div class="image-title-wrap">
                                            <button type="button" onclick="removeUpload()" class="remove-image "
                                                jf-ext-button-ct="remove uploaded image">Remove <span
                                                    class="image-title">Uploaded Image</span></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <span class="or-text">Or</span>
                                    <button class="file-upload-btn font-regular" type="button"
                                        onclick="$('.file-upload-input').trigger( 'click' )"
                                        jf-ext-button-ct="browse">Browse</button>
                                </div>
                                <div class="form-floating mt-5 gx-5">
                                    <button id="btn_apply_now" type="button" class="btn btn-primary btn-default "
                                        jf-ext-button-ct="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>