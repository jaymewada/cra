<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">CAPABILITIES</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Sustainability Solutions</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Sustainability Solutions</h1>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>