 
   mapboxgl.accessToken = 'pk.eyJ1IjoiY2ljZXJvYWdlbnR1ciIsImEiOiJja2lyOTBuOXYwOGJ5MnhzY2kyMXRocG9nIn0.fYRg2TLIuWCaZuxVjhsadg';

   /**
    * Add the map to the page
    */
   const map = new mapboxgl.Map({
     container: 'map',
     style: 'mapbox://styles/mapbox/light-v10',
     center: [72.865356, 19.042816],
     zoom: 5,
     scrollZoom: false
   });
   
   const stores = {
     'type': 'FeatureCollection',
     
     'features': [
       {
         'type': 'Feature',
         'geometry': {
           'type': 'Point',
           'coordinates': [72.865356, 19.042816]
         },
         'properties': {
           'name': 'HQ, Mumbai', 
           
           'street': 'care@careedge.in',
           'plz': '+91-22-6754 3456   |   +91-022-6754 3457',
           'city': '4th Floor, Godrej Coliseum, Somaiya Hospital Road, Off Eastern Express Highway, Sion (East), Mumbai - 400 022.'
         }
       },
       {
          'type': 'Feature',
          'geometry': {
              'type': 'Point',
              'coordinates': [72.865434, 19.044249]
           },
        'properties': {
          'name': 'Andheri - Mumbai',
          'plz': '+91-22-6837 4400',
          'street': 'care@careedge.in',
          'city': 'A Wing - 1102 / 1103, Kanakia Wall Street, Andheri Kurla Road,Chakala, Andheri (E), Mumbai - 400 093.'
        }
      },
      {
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [72.503902, 23.009394]
        },
        'properties': {
          'name': 'Ahmedabad',
          'plz': '+91 7940265656   |   +91 7940265657',
          'street': 'deepak.prajapati@careedge.in',
          'city': '32, Titanium, Prahaladnagar Corporate Road, Satellite, Ahmedabad - 380 015.'
        }
      },
       {
         'type': 'Feature',
         'geometry': {
           'type': 'Point',
           'coordinates': [77.612532, 12.973604]
         },
         'properties': {
           'name': 'Bengaluru',
           'plz': '+91 9632933990   |   +91 80-46625555',
           'street': 'nitin.dalmia@careedge.in',
           'city': 'Unit No. 205 -208, 2nd Floor, Prestige Meridian 1, No. 30, M. G. Road, Bengaluru, Karnataka 560001'
         }
       },
      {
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [80.276320, 13.072952]
        },
        'properties': {
          'name': 'Chennai',
          'plz': '044-28490876/0811/7812',
          'street': 'pradeep.kumar@careedge.in',
          'city': 'Unit No. O-509/C, Spencer Plaza, 5th Floor, No. 769, Anna Salai, Chennai 600 002'
        }
      },
      {
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [76.991999, 11.004464]
        },
        'properties': {
          'name': 'Coimbatore',
          'plz': '+91 0422 - 4332399  |   0422 - 4502399',
          'street': 'fril.kumar@careedge.in',
          'city': 'T-3, 3rd Floor, Manchester Square, Puliakulam Road, Coimbatore- 641037'
        }
      },
      {
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [78.483456, 17.403968]
        },
        'properties': {
          'name': 'Hyderabad',
          'plz': '+91-040 40102030   |   9052000521',
          'street': 'ramesh.bob@careedge.in',
          'city': '401, Ashoka Scintilla, 3-6-520, Himayat Nagar, Hyderabad - 500 029,'
        }
      },
      {
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [88.356619, 22.544790]
        },
        'properties': {
          'name': 'Kolkata',
          'plz': '+91-033- 40181600  |   22831803',
          'street': 'priti.agarwal@careedge.in',
          'city': '3rd Floor, Prasad Chambers (Shagun Mall Building),10A, Shakespeare Sarani, Kolkata - 700 071'
        }
      },
      {
        'type': 'Feature',
        'geometry': {
          'type': 'Point',
          'coordinates': [77.446458,28.607068]
        },
        'properties': {
          'name': 'New Delhi',
          'plz': '+91 120 4452000',
          'street': 'dinesh.sharma@careedge.in',
          'city': 'Plot no. C-001 A/2 Sector 16B, Berger Tower, Noida, Gautam Budh Nagar (UP) - 201301'
        }
      },
      {
         'type': 'Feature',
         'geometry': {
           'type': 'Point',
           'coordinates': [73.841853, 18.525504]
         },
         'properties': {
           'name': 'Pune',
           'plz': '+91-020-40009000   |   8106400001',
           'street': 'aakash.jain@careedge.in',
           'city': '9th Floor, Pride Kumar Senate, Bhamburda, Senapati Bapat Road, Shivaji Nagar, Pune-411015'
         }
       }
      
     ]
   };
   
   /**
    * Assign a unique id to each store. You'll use this `id`
    * later to associate each point on the map with a listing
    * in the sidebar.
    */
   stores.features.forEach((store, i) => {
     store.properties.id = i;
   });
   
   /**
    * Wait until the map loads to make changes to the map.
    */
   map.on('load', () => {
     /**
      * This is where your '.addLayer()' used to be, instead
      * add only the source without styling a layer
      */
     map.addSource('places', {
       'type': 'geojson',
       'data': stores
     });
   
     /**
      * Add all the things to the page:
      * - The location listings on the side of the page
      * - The markers onto the map
      */
     buildLocationList(stores);
     addMarkers();
   });
   
   /**
    * Add a marker to the map for every store listing.
    **/
   function addMarkers() {
     /* For each feature in the GeoJSON object above: */
     for (const marker of stores.features) {
       /* Create a div element for the marker. */
       const el = document.createElement('div');
       /* Assign a unique `id` to the marker. */
       el.id = `marker-${marker.properties.id}`;
       /* Assign the `marker` class to each marker for styling. */
       el.className = 'marker';
   
       /**
        * Create a marker using the div element
        * defined above and add it to the map.
        **/
       new mapboxgl.Marker(el, { offset: [0, -23] })
         .setLngLat(marker.geometry.coordinates)
         .addTo(map);
   
       /**
        * Listen to the element and when it is clicked, do three things:
        * 1. Fly to the point
        * 2. Close all other popups and display popup for clicked store
        * 3. Highlight listing in sidebar (and remove highlight for all other listings)
        **/
       el.addEventListener('click', (e) => {
         /* Fly to the point */
         flyToStore(marker);
         
         /* Close all other popups and display popup for clicked store */
         createPopUp(marker);
         /* Highlight listing in sidebar */
         const activeItem = document.getElementsByClassName('active');
         console.log(activeItem);
         e.stopPropagation();
         if (activeItem[0]) {
           activeItem[0].classList.remove('active');
         }
         const listing = document.getElementById(
           `listing-${marker.properties.id}`
         );
         listing.classList.add('active');
       });
     }
   }
   
   /**
    * Add a listing for each store to the sidebar.
    **/
   function buildLocationList(stores) {
     for (const store of stores.features) {
       /* Add a new listing section to the sidebar. */
       const listings = document.getElementById('listings');
       const listing = listings.appendChild(document.createElement('div'));
       /* Assign a unique `id` to the listing. */
       listing.id = `listing-${store.properties.id}`;
       /* Assign the `item` class to each listing for styling. */
       listing.className = 'item';
   
       /* Add the link to the individual listing created above. */
       const link = listing.appendChild(document.createElement('a'));
       link.href = '#';
       link.className = 'title';
       link.id = `link-${store.properties.id}`;
       link.innerHTML = `${store.properties.name}`;
   
       /* Add details to the individual listing. */
       const details = listing.appendChild(document.createElement('div'));
       details.innerHTML = `<p class="nameT"> <span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" viewBox="0 0 24 24">
                                       <defs>
                                          <clipPath id="clip-path">
                                             <rect id="Rectangle_4128" data-name="Rectangle 4128" width="16.853" height="23.6" fill="#fff"></rect>
                                          </clipPath>
                                       </defs>
                                       <g id="Contact" transform="translate(-13 -13)">
                                          <g id="Icons_Contact_ic-contact-phone" data-name="Icons / Contact / ic-contact-phone" transform="translate(13 13)">
                                             <rect id="Rectangle_193" data-name="Rectangle 193" width="24" height="24" fill="none"></rect>
                                             <g id="Group_26886" data-name="Group 26886" transform="translate(3.573 0.2)">
                                                <g id="Group_26885" data-name="Group 26885" clip-path="url(#clip-path)">
                                                   <path id="Path_12726" data-name="Path 12726" d="M8.159,0A8.515,8.515,0,0,1,16.8,7.424a10.675,10.675,0,0,1-.757,5.021,27.364,27.364,0,0,1-3.484,6.343c-1.064,1.49-2.22,2.915-3.356,4.352-.489.618-1.019.6-1.532.006A43.187,43.187,0,0,1,1.607,14.26,15.746,15.746,0,0,1,.087,9.607,8.451,8.451,0,0,1,8.159,0m.251,21.393c.14-.158.239-.257.323-.368.907-1.193,1.855-2.358,2.706-3.588a24.613,24.613,0,0,0,3.224-6.265A7.755,7.755,0,0,0,15.04,7.11,6.7,6.7,0,0,0,5,2.642,6.442,6.442,0,0,0,1.741,9.071a11.766,11.766,0,0,0,1.143,3.862,32.049,32.049,0,0,0,3.643,6.075c.6.8,1.235,1.566,1.884,2.385" transform="translate(0 0)" fill="#fff"></path>
                                                   <path id="Path_12727" data-name="Path 12727" d="M13.486,10.121a3.369,3.369,0,1,1-3.347-3.384,3.382,3.382,0,0,1,3.347,3.384m-1.685,0a1.684,1.684,0,1,0-1.7,1.673,1.673,1.673,0,0,0,1.7-1.673" transform="translate(-1.683 -1.681)" fill="#fff"></path>
                                                </g>
                                             </g>
                                          </g>
                                       </g>
                                    </svg></span> ${store.properties.street}</p>
   <p>${store.properties.plz}</p
   <p>${store.properties.city}</p>`;
   
   
       /**
        * Listen to the element and when it is clicked, do four things:
        * 1. Update the `currentFeature` to the store associated with the clicked link
        * 2. Fly to the point
        * 3. Close all other popups and display popup for clicked store
        * 4. Highlight listing in sidebar (and remove highlight for all other listings)
        **/
       link.addEventListener('click', function () {
         for (const feature of stores.features) {
           if (this.id === `link-${feature.properties.id}`) {
             flyToStore(feature);
             createPopUp(feature);
           }
         }
         
         if (document.querySelector('#listings div.active') !== null) {
            document.querySelector('#listings div.active').classList.remove('active');
          }

         this.parentNode.classList.add('active');
       });
     }
   }
   
   /**
    * Use Mapbox GL JS's `flyTo` to move the camera smoothly
    * a given center point.
    **/
   function flyToStore(currentFeature) {    
     map.flyTo({
       center: currentFeature.geometry.coordinates,
       zoom: 15
     });
   }
   
   /**
    * Create a Mapbox GL JS `Popup`.
    **/
   function createPopUp(currentFeature) {
     const popUps = document.getElementsByClassName('mapboxgl-popup');
     if (popUps[0]) popUps[0].remove();
     const popup = new mapboxgl.Popup({ closeOnClick: false })
       .setLngLat(currentFeature.geometry.coordinates)
       .setHTML(
         `<h3>${currentFeature.properties.name}</h3><p>${currentFeature.properties.street}</p>
   <p>${currentFeature.properties.plz}</p>
   <p>${currentFeature.properties.city}</p>`
       )
       .addTo(map);
   }
   
   
   
   
   const geocoder = new MapboxGeocoder({
     // Initialize the geocoder
     accessToken: mapboxgl.accessToken, // Set the access token
     mapboxgl: mapboxgl, // Set the mapbox-gl instance
     marker: false, // Do not use the default marker style
     placeholder: 'Search for places in Berkeley', // Placeholder text for the search bar
     bbox: [6.441593,47.458186,14.919434,54.762621], // Boundary for Germany
     proximity: {
       longitude: 8.648086,
       latitude: 50.107011
     } // Coordinates of UC Berkeley
   });
   
   // Add the geocoder to the map
   map.addControl(geocoder);
   
   // After the map style has loaded on the page,
   // add a source layer and default styling for a single point
   map.on('load', () => {
     map.addSource('single-point', {
       'type': 'geojson',
       'data': {
         'type': 'FeatureCollection',
         'features': []
       }
     });
   
     map.addLayer({
       'id': 'point',
       'source': 'single-point',
       'type': 'circle',
       'paint': {
         'circle-radius': 10,
         'circle-color': '#448ee4'
       }
     });
   
     // Listen for the `result` event from the Geocoder // `result` event is triggered when a user makes a selection
     //  Add a marker at the result's coordinates
     geocoder.on('result', (event) => {
       map.getSource('single-point').setData(event.result.geometry);
     });
   });
   