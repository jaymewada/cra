<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active">Get Rated</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Get Rated</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100"></section>


<?php include "components/footer.php" ?>