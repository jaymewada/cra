<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">CAPABILITIES</a></li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">INFRASTRUCTURE SECTOR</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">INFRASTRUCTURE SECTOR</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 " style="background-color: #F0F0F0; ">
    <div class="container-fluid" id="renderHtmlOverviewCapabilitySectiondata">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-top">
                    <div class="col-md-12 col-xl-12">
                        <h3 class="heading-1 text-black ">Overview</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <p>CRAF’s Infrastructure Sector Ratings (ISR) encompass ratings assigned to debt programs of
                            issuers in the thermal power, solar energy, wind energy, hydro energy, transmission, power
                            distribution companies, roads (toll, annuity, and hybrid annuity), telecommunications,
                            airports, ports, and other such infrastructure-related sectors. CRAF adopts a separate
                            methodology for evaluation and assignment of ISR, distinct from corporate sector debt
                            ratings as rating drivers for ISR are quite different from that of the corporate sector.
                        </p>
                    </div>
                    <div class="col-md-8 col-xl-5 offset-xl-1 mt-3 mt-xl-0">
                        <div class="row g-4">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>