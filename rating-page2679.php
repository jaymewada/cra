<style>
.latest-area {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    margin: 140px 0;
    padding: 0 78px;
}

.latest-Inn {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    margin-top: 61px;
}

.latest-Inn-item {
    width: 100%;
    background: #F3F3F3;
    text-align: left;
    letter-spacing: -0.72px;
    color: #1C304C;
    opacity: 1;
    font-family: 'proxima_novaregular';
    font-size: 24px;
    line-height: 1.2;
    padding: 25px 31px 25px 40px;
    margin-bottom: 10px;
    justify-content: space-between;
    display: flex;
    flex-wrap: wrap;
}

.latest-Inn-item a {
    color: #1C304C;
    justify-content: space-between;
    display: flex;
    flex-wrap: wrap;
    width: 100%;
}

.latest-Inn td a {
    text-align: left;
    letter-spacing: 0px;
    color: #000000;
    font-size: 18px;
    line-height: 1.1;
    opacity: 1;
    font-family: 'proxima_novaregular';
    white-space: nowrap;
}

.latest-Inn-item .arrowLink {
    background-color: #1C304C;
    border-radius: 100%;
    width: 35px;
    height: 35px;
    display: flex;
    align-items: center;
    justify-content: center;
}

.latest-Inn thead,
.latest-Inn td,
.latest-Inn th {
    padding: 30px 60px;
    border: 0;
}

.latest-Inn th {
    text-align: left;
    letter-spacing: 0px;
    color: #000000;
    font-size: 18px;
    line-height: 1.1;
    opacity: 1;
    text-transform: capitalize;
    font-size: 24px;
    line-height: 1.2;
    opacity: 1;
    white-space: nowrap;
}

.latest-Inn td {
    text-align: left;
    letter-spacing: 0px;
    color: #000000;
    font-size: 18px;
    line-height: 1.1;
    opacity: 1;
    font-family: 'proxima_novaregular';
    white-space: nowrap;
}

.latest-Inn .table> :not(:first-child) {
    border-top: 0px solid currentColor;
}

.latest-Inn .pagination-div {
    width: 100%;
}

.latest-Inn .table-responsive {
    width: 100%;
}


.latest-Inn thead,
.latest-Inn td,
.latest-Inn th {
    padding: 30px 60px !important;
    border: 0;
}

.pagina {
    padding-bottom: 30px
}

.pagina svg {
    width: 30px;
}

.pagina .flex-1:first-child {
    /* margin-bottom: 10px; */
    display: none;
}

.pagina .text-gray-700:first-child {
    /* margin-bottom: 10px; */
    display: none;
}

.latest-Inn select {
    padding: 12px 10px;
}

.latest-Inn button {
    background-color: var(--primary_color);
    color: #fff;
}

.pagina {
    margin-top: -100px;
}

@media (max-width:1599px) {

    .latest-Inn th {
        padding: 20px 30px;
        font-size: 18px;
    }

    .latest-Inn td {
        padding: 20px 30px;
        font-size: 18px;
    }

    .latest-area {
        margin: 120px 0;
        padding: 0 55px;
    }

    .latest-Inn {
        margin-top: 40px;
    }

    .latest-Inn-item {
        font-size: 21px;
        padding: 20px 25px 20px 30px;
    }

    .latest-Inn-item a {
        width: 100%;
    }

    .latest-Inn-item {
        font-size: 18px;
    }

    .latest-Inn-item span {
        width: 80%;
    }

    .latest-Inn-item .arrowLink {
        width: 30px;
        height: 30px;
    }

}



@media (max-width:768px) {

    .latest-area {
        margin: 55px 0;
        padding: 0;
    }

    .latest-Inn th {
        padding: 10px 13px;
        font-size: 13px;
    }

    .latest-Inn td {
        padding: 10px 13px;
        font-size: 13px;
    }

    .latest-Inn {
        margin-top: 15px;
    }



}
</style>

<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from www.careratingsafrica.com/rating-page?page=1 by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 23 Apr 2024 18:51:52 GMT -->
    <!-- Added by HTTrack -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="vZWAu33NBWwhpop4yuJApHVSQqcyBlZ6H9vghL4i">
        <meta property="og:image"
            content="https://www.careratings.com/storage/app/admin/images/banner-2_1679044184_1679045152.jpg">
        <link rel="shortcut icon" href="public/frontend-assets/images/favicon.png" type="image/x-icon" />
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/themes/base/jquery-ui.min.css"
            integrity="sha512-ELV+xyi8IhEApPS/pSj66+Jiw+sOT1Mqkzlh8ExXihe4zfqbWkxPRi8wptXIO9g73FSlhmquFlUOuMSoXz5IRw=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="public/frontend-assets/css/all.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/swiper-bundle.min.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/style.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/new-custom-backend.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/responsive.css" rel="stylesheet" />

        <link href="public/frontend-assets/css/mapbox-gl.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/mapbox-gl-geocoder.css" rel="stylesheet" />

        <link href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.1.1/css/fontawesome.min.css"
            rel="stylesheet">
        <link rel='stylesheet'
            href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
        <!-- Docs styles -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/CDNSFree2/Plyr/plyr.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
        <link rel="stylesheet" href="public/app-assets/assets/css/bootstrap-datepicker.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
        <!-- Google tag (gtag.js) -->

        <script async src=https://www.googletagmanager.com/gtag/js?id=UA-214539321-1></script>

        <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());



        gtag('config', 'UA-214539321-1');
        </script>
        <title>CareEdge | Africa</title>
        <style>
        img#loader {
            max-width: 110px;
            margin: 20px auto 0;
        }

        img#loader_contactus {
            max-width: 110px;
            margin: 20px auto 0;
        }

        ul.parsley-errors-list {
            list-style: none;
        }

        .parsley-required,
        .parsley-pattern,
        .parsley-maxlength,
        .parsley-custom-error-message,
        .parsley-minlength,
        .parsley-type,
        .parsley-length,
        .parsley-equalto .error-sign {
            color: #FF0000;
        }


        .badge {
            padding-left: 9px;
            padding-right: 9px;
            -webkit-border-radius: 9px;
            -moz-border-radius: 9px;
            border-radius: 9px;
        }

        .label-warning[href],
        .badge-warning[href] {
            background-color: #c67605;
        }

        .shopping-cart-count {
            font-size: 12px;
            background: #ff0000;
            color: #fff;
            padding: 0 5px;
            vertical-align: top;
            margin-left: -16px;
            margin-top: -30px;
            width: 20px;
            height: 20px;
            text-align: center;
            line-height: 10px;
            padding: 5px 0px;
            border-radius: 50px;
        }

        .register-text {
            margin-top: 30px;
            padding: 16px 54px;
            background-color: #00d3c3;
            text-decoration: none;
            color: #fff;
            font-family: 'ProximaNova-Semibold' !important;
            font-size: 14px;
        }

        .register-text:hover {
            color: #fff;
        }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <title>CareEdge | Ratings</title>
    </head>

    <body>
        <!-- Desktop Header -->
        <header class="desktop_header">
            <div class="top_header container-fluid desktop-top_header">
                <ul class="top-list left-nav">
                    <li><a href="https://www.careedge.in/" target="_blank"> CareEdge Group</a></li>
                    <li>
                        <a href="https://www.careratings.com/" target="_blank">
                            CareEdge Ratings
                        </a>
                    </li>
                    <li>
                        <a href="http://careedgeadvisory.com/" target="_blank">
                            CareEdge Advisory
                        </a>
                    </li>
                    <li>
                        <a href="https://caapl.in/" target="_blank">
                            CareEdge Analytics
                        </a>
                    </li>
                    <li>
                        <a href="https://www.careratingsnepal.com/" target="_blank">
                            CareEdge Nepal
                        </a>
                    </li>
                    <li class="active">
                        <a href="index.html" target="_blank">
                            CareEdge Africa
                        </a>
                    </li>
                </ul>
                <ul class="list-inline m-0 right-nav">
                    <li class="list-inline-item">
                        <select name="country" id="country" class="">
                            <option value="1">Mauritius</option>
                        </select>
                    </li>
                    <li class="list-inline-item">
                        <a href="careers.html">Careers</a>
                    </li>
                    <!-- <li class="list-inline-item">
                    <a href="https://www.careratingsafrica.com/support" class="m-0">Contact</a>
                </li> -->
                </ul>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light desktop-menu">
                <div class="container-fluid h-100 pe-0">

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"
                        onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))">
                        <svg width="100" height="100" viewBox="0 0 100 100">
                            <path class="line line1"
                                d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058">
                            </path>
                            <path class="line line2" d="M 20,50 H 80"></path>
                            <path class="line line3"
                                d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942">
                            </path>
                        </svg>
                    </button>

                    <a class="navbar-brand" href="index.html">
                        <img src="public/frontend-assets/images/logo_careedge_ratings_africa.png"
                            alt="CareEdge_ratings_logo" width="55%" />
                    </a>
                    <button type="button" class="loginCta">
                        <span class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-person" viewBox="0 0 16 16">
                                <path
                                    d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                            </svg>
                        </span>
                    </button>

                    <!--DESKTOP MENU-->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Find Ratings
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="nav-link" href="find-ratings.html">Find Ratings</a></li>
                                    <li><a class="nav-link" href="rating-page.html">Ratings List</a></li>



                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Get Rated
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="rating_process.html">Rating Process</a></li>
                                    <li><a class="dropdown-item" href="criteria_methodologies.html">Criteria /
                                            Methodologies</a></li>
                                    <li><a class="dropdown-item" href="rating_faqs.html">Rating FAQs</a></li>
                                    <li><a class="dropdown-item" href="fee_structure.html">Fee Structure</a></li>
                                    <li><a class="dropdown-item" href="rating-symbols-and-definition.html">Rating
                                            Symbols & Definition</a></li>

                                    <li><a class="dropdown-item" href="regulatory-guidelines.html">Regulatory
                                            Guidelines</a></li>
                                    <li><a class="dropdown-item" href="rating-committee.html">Rating Committee</a></li>


                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    CAPABILITIES
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="corporate-sector.html">CORPORATE SECTOR</a></li>
                                    <li><a class="dropdown-item" href="financial-sector.html">FINANCIAL SECTOR</a></li>
                                    <li>
                                        <a class="dropdown-item" href="structured-finance.html">STRUCTURED FINANCE</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="infrastructure-sector.html">INFRASTRUCTURE
                                            SECTOR</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sustainability-solutions.html">SUSTAINABILITY
                                            SOLUTIONS</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sme-solutions.html">SME SOLUTIONS</a>
                                    </li>
                                    <!--<li>
                                    <a class="dropdown-item" href="https://www.careratingsafrica.com/other-rating-products">OTHER RATING PRODUCTS</a>
                                </li>-->
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    INSIGHTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light dropdown-display dropdown-newDegn"
                                    aria-labelledby="navbarDropdown">
                                    <li>
                                        <h5>economy</h5>
                                        <ul class="dropDown-inner">
                                            <li><a class="dropdown-item" href="economy-updates.html">Economy Updates</a>
                                            </li>

                                            <li><a class="dropdown-item" href="debt-market-update.html">Debt Market
                                                    Update</a></li>
                                            <li><a class="dropdown-item" href="bond-market-update.html">Bond Market
                                                    Update</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h5>industry research</h5>
                                        <ul class="dropDown-inner">
                                            <li><a class="dropdown-item" href="industry-research.html">Overview</a></li>
                                            <li><a class="dropdown-item" href="industry/Industry%20research/3.html"></a>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown hide-in-s-lap show-in-s-lap">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    MEDIA & EVENTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="events-and-webinars.html">Events and Webinars</a>
                                    </li>
                                    <li><a class="dropdown-item" href="media-coverage.html">Media Coverage</a></li>
                                    <li><a class="dropdown-item" href="publications.html">Publications</a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Investors
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">

                                    <!--<li><a class="dropdown-item" href="https://www.careratingsafrica.com/financial-performance">Financial Performance</a></li>-->
                                    <li><a class="dropdown-item" href="shareholding-patterns.html">Shareholding
                                            Pattern</a></li>
                                    <li><a class="dropdown-item" href="annual-reports.html">Annual Reports</a></li>
                                    <li><a class="dropdown-item" href="corporate-governance">Corporate Governance</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item hide-in-s-lap show-in-s-lap">
                                <a class="nav-link" href="about-us">About us</a>
                            </li>
                            <li class="nav-item hide-in-s-lap show-in-s-lap">
                                <a class="nav-link" href="contactus">Contact Us</a>
                            </li>
                        </ul>
                        <div class="secondary-menu">
                            <button class="menu-icon"
                                onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))"
                                aria-label="Main Menu">
                                <svg width="100" height="100" viewBox="0 0 100 100">
                                    <path class="line line1"
                                        d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                                    <path class="line line2" d="M 20,50 H 80" />
                                    <path class="line line3"
                                        d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                                </svg>
                            </button>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)" class="dropdown-toggle">
                                        MEDIA & EVENTS</a>
                                    <ul class="secondary-menu-list ">
                                        <li><a href="events-and-webinars.html">Events and Webinars</a></li>
                                        <li><a href="media-coverage.html">Media Coverage</a></li>
                                        <li><a class="dropdown-item" href="publications.html">Publications</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="about-us">
                                        ABOUT US</a>
                                </li>
                                <li>
                                    <a href="contactus">
                                        CONTACT US</a>
                                </li>
                            </ul>
                        </div>
                        <div class=" d-none d-xl-block ">
                            <ul class="search-call-icon">
                                <li>

                                    <div class="searchbar1">
                                        <input type="text" placeholder="Find Ratings / Company / Research"
                                            id="CompanyInput" oninput="myFunc(this.value)" value=""
                                            class="searchBarInput">
                                        <div class="search"></div>
                                    </div>

                                    <!--a href="javascript:void(0)">
                                    <svg id="Icons_Actions_ic-actions-search" data-name="Icons / Actions / ic-actions-search" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <rect id="Rectangle_157" data-name="Rectangle 157" width="24" height="24" fill="none"></rect>
                                        <g id="ic-actions-search" transform="translate(3.62 3.55)">
                                            <circle id="Ellipse_12" data-name="Ellipse 12" cx="7" cy="7" r="7" transform="translate(0)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.5">
                                            </circle>
                                            <line id="Line_50" data-name="Line 50" x2="4.88" y2="4.88" transform="translate(11.88 12.02)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.5">
                                            </line>
                                        </g>
                                    </svg>
                                </a-->
                                </li>
                                <!-- <li>
                                <a href="javascript:void(0)">
                                    <svg id="Icons_Contact_ic-contact-phone" data-name="Icons / Contact / ic-contact-phone" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <rect id="Rectangle_193" data-name="Rectangle 193" width="24" height="24" fill="none"></rect>
                                        <g id="ic-contact-phone" transform="translate(3.352 3.346)">
                                            <path id="Path_89" data-name="Path 89" d="M3.75,6.087,6.14,3.7A2,2,0,0,1,9,3.7l1.36,1.36a2,2,0,0,1,0,2.83l-.89.9h0a16.48,16.48,0,0,0,5.64,5.65h0l.89-.9a2,2,0,0,1,2.83,0l1.34,1.33a2,2,0,0,1,0,2.83l-2.39,2.41a1,1,0,0,1-1.26.13h0A46,46,0,0,1,3.63,7.347h0A1,1,0,0,1,3.75,6.087Z" transform="translate(-3.461 -3.095)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
                                            </path>
                                        </g>
                                    </svg>
                                </a>
                            </li> -->
                                <li>
                                    <a href="shoppingcart.html">
                                        <!-- <i class="fa" style="font-size:24px">&#xf07a;</i>-->
                                        <img src="public/frontend-assets/images/Cart_Icon.svg">
                                        <span class='badge badge-warning shopping-cart-count'>0</span>
                                    </a>
                                </li>
                                <li>
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="login-cta">Login/Register</div> -->
                        <a class="btn btn-primary login-cta" data-bs-toggle="modal" href="#exampleModalToggle"
                            role="button">Login/Register</a>
                    </div>

                </div>
            </nav>
        </header>
        <!--Mobile menu -->
        <!--Mobile menu -->
        <header class="mobile_header">
            <!-- <div class="top_header container-fluid font-medium mobile-top_header">
            <li class="sub-links">
              <a href="javascript:void(0)">CARE Group</a>
                <ul>
                  <li><a href="http://qc.evolutionco.in/CareEdge-ratings/">CARE Ratings</a></li>
                  <li><a href="javascript:void(0)">CARE Advisory & Research</a></li>
                  <li><a href="javascript:void(0)">CARE Risk Solutions</a></li>
                  <li><a href="javascript:void(0)">CARE Ratings Nepal</a></li>
                  <li><a href="javascript:void(0)">CARE Ratings Africa</a></li>
                </ul>
            </li>
          </div> -->
            <div class="top_header container-fluid font-medium mobile-top_header">
                <form>
                    <!-- <select name="Others Sites" onchange="window.open(this.value, '_blank')"> -->
                    <select name="Others Sites" onchange="location = this.value;">
                        <option value="https://www.careedge.in/"> CareEdge Group </option>
                        <option value="https://www.careratingsindia.com/">CareEdge Ratings</option>
                        <option value="http://careedgeadvisory.com/">CareEdge Advisory & Research</option>
                        <option value="https://www.carerisksolutions.com/">CareEdge Risk Solutions</option>
                        <option value="https://careratingsnepal.com/">CareEdge Nepal</option>
                        <option value="http://www.careratingsafrica.com/" selected>CareEdge Africa</option>
                    </select>
                </form>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light mobile-menu">
                <div class="container-fluid h-100 pe-0">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"
                        onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))">
                        <svg width="100" height="100" viewBox="0 0 100 100">
                            <path class="line line1"
                                d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058">
                            </path>
                            <path class="line line2" d="M 20,50 H 80"></path>
                            <path class="line line3"
                                d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942">
                            </path>
                        </svg>
                    </button>
                    <a class="navbar-brand" href="index.html">
                        <img src="public/frontend-assets/images/logo_careedge_ratings_africa.png"
                            alt="CareEdge_ratings_logo" />
                    </a>
                    <div class=" d-xl-block call-login-icon">
                        <ul class="search-call-icon">
                            <li>
                                <div class="searchbar1">
                                    <input type="text" placeholder="Find Ratings / Company / Research"
                                        id="CompanyInputmobile" oninput="myFunc(this.value)" value=""
                                        class="searchBarInput">
                                    <div class="search"></div>
                                </div>

                            </li>
                            <!-- <li>
                            <a href="javascript:void(0)">
                                <svg id="Icons_Contact_ic-contact-phone" data-name="Icons / Contact / ic-contact-phone"
                                    xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <rect id="Rectangle_193" data-name="Rectangle 193" width="24" height="24"
                                        fill="none"></rect>
                                    <g id="ic-contact-phone" transform="translate(3.352 3.346)">
                                        <path id="Path_89" data-name="Path 89"
                                            d="M3.75,6.087,6.14,3.7A2,2,0,0,1,9,3.7l1.36,1.36a2,2,0,0,1,0,2.83l-.89.9h0a16.48,16.48,0,0,0,5.64,5.65h0l.89-.9a2,2,0,0,1,2.83,0l1.34,1.33a2,2,0,0,1,0,2.83l-2.39,2.41a1,1,0,0,1-1.26.13h0A46,46,0,0,1,3.63,7.347h0A1,1,0,0,1,3.75,6.087Z"
                                            transform="translate(-3.461 -3.095)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"></path>
                                    </g>
                                </svg>
                            </a>
                        </li> -->
                        </ul>
                    </div>
                    <button class="loginCta" data-bs-toggle="modal" href="#exampleModalToggle" role="button">
                        <span class="">
                            <img src="public/frontend-assets/images/icons/noun-login.svg">
                        </span>
                    </button>
                    <!-- <a class="btn btn-primary loginCta" data-bs-toggle="modal" href="#exampleModalToggle_mobile"
                        role="button"><span class="">
                        <img src="images/icons/noun-login.svg">
                    </span></a> -->



                    <!--MOBILE MENU-->


                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav dropdown ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="find-ratings.html">Find Ratings</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Get Rated
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="rating_process.html">Rating Process</a></li>
                                    <li><a class="dropdown-item" href="criteria_methodologies.html">Criteria /
                                            Methodologies</a></li>
                                    <li><a class="dropdown-item" href="rating_faqs.html">Rating FAQs</a></li>
                                    <li><a class="dropdown-item" href="fee_structure.html">Fee Structure</a></li>
                                    <li><a class="dropdown-item" href="rating-symbols-and-definition.html">Rating
                                            Symbols & Definition</a></li>

                                    <li><a class="dropdown-item" href="regulatory-guidelines.html">Regulatory
                                            Guidelines</a></li>


                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    CAPABILITIES
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="corporate-sector.html">CORPORATE SECTOR</a></li>
                                    <li><a class="dropdown-item" href="financial-sector.html">FINANCIAL SECTOR</a></li>
                                    <li>
                                        <a class="dropdown-item" href="structured-finance.html">STRUCTURED FINANCE</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="infrastructure-sector.html">INFRASTRUCTURE
                                            SECTOR</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sustainability-solutions.html">SUSTAINABILITY
                                            SOLUTIONS</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sme-solutions.html">SME SOLUTIONS</a>
                                    </li>
                                    <!--<li>
                                    <a class="dropdown-item" href="https://www.careratingsafrica.com/other-rating-products">OTHER RATING PRODUCTS</a>
                                </li>-->
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    INSIGHTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light dropdown-display dropdown-newDegn"
                                    aria-labelledby="navbarDropdown">
                                    <li>
                                        <h5>economy</h5>
                                        <ul class="dropDown-inner">
                                            <li><a class="dropdown-item" href="economy-updates.html">Economy Updates</a>
                                            </li>

                                            <li><a class="dropdown-item" href="debt-market-update.html">Debt Market
                                                    Update</a></li>
                                            <li><a class="dropdown-item" href="bond-market-update.html">Bond Market
                                                    Update</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h5>industry research</h5>
                                        <ul class="dropDown-inner">
                                            <li><a class="dropdown-item" href="industry-research.html">Overview</a></li>
                                            <li><a class="dropdown-item" href="industry/Industry%20research/3.html"></a>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown hide-in-s-lap show-in-s-lap">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    MEDIA & EVENTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="events-and-webinars.html">Events and Webinars</a>
                                    </li>
                                    <li><a class="dropdown-item" href="media-coverage.html">Media Coverage</a></li>
                                    <li><a class="dropdown-item" href="publications.html">Publications</a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Investors
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">

                                    <!--<li><a class="dropdown-item" href="https://www.careratingsafrica.com/financial-performance">Financial Performance</a></li>-->
                                    <li><a class="dropdown-item" href="shareholding-patterns.html">Shareholding
                                            Pattern</a></li>
                                    <li><a class="dropdown-item" href="annual-reports.html">Annual Reports</a></li>
                                    <li><a class="dropdown-item" href="corporate-governance">Corporate Governance</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="about-us">About US</a>

                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="contactus">Contact US</a>

                            </li>

                        </ul>
                        <div class="bg-white extra-menu">
                            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                                <!-- <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Careers</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Support</a>
                            </li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <!-- Modal popup for desktop-->
        <div class="modal fade login-popup" id="exampleModalToggle" aria-hidden="true"
            aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalToggleLabel">Subscribe for the Rating Rational</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                    </div>
                    <div class="modal-body">
                        <div class="login-regi-form">
                            <form class="row form-style-1" name="get_otp_login_form" id="get_otp_login_form">
                                <div class="row">
                                    <div class="alert alert-success print-success-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                    <div class="alert alert-danger print-error-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="email" name="EMAILADDRESS" id="EMAILADDRESS" class="form-control"
                                            placeholder="name@example.com" value="" required
                                            data-parsley-trigger="focusout" data-parsley-required-message="Enter Email">
                                        <label for="floatingInput">Email ID*</label>
                                    </div>
                                </div>
                                <!-- <div class="col-md-12">
                                <div class="form-floating">
                                    <input type="text" name="PHONENUMBER" id="PHONENUMBER" class="form-control" placeholder="1234567890" required value="9822540715" data-parsley-trigger="focusout" data-parsley-required-message="Enter Phone Number">
                                    <label for="floatingInput">Enter Phone Number</label>
                                </div>
                            </div> -->
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input id="password-field" type="password" class="form-control" name="PASSWORD"
                                            placeholder="password" required data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Your Password">
                                        <span toggle="#password-field"
                                            class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        <label for="floatingInput">Password*</label>

                                    </div>
                                </div>
                                <input type="hidden" name="_token" id="token_get_otp"
                                    value="vZWAu33NBWwhpop4yuJApHVSQqcyBlZ6H9vghL4i">
                                <button type="button" class="btn btn-primary mt-3 view-report-btn font-semi-bold"
                                    id="get_otp_login_btn">Login with OTP</button>
                                <div class="content d-flex justify-content-between p-0">
                                    <a href="forgot_password.html" class="btn-link register-text">Forgot Password</a>
                                    <a href="register.html" class="btn-link ms-3 register-text">Register</a>
                                </div>
                                <img id="loader" src="public/frontend-assets/images/loader.gif">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade login-otp" id="exampleModalToggle2" aria-hidden="true"
            aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="container height-100 d-flex justify-content-center align-items-center">
                    <form name="otp_validate_form" data-parsley-validate="" id="otp_validate_form" class="digit-group"
                        data-autosubmit="false" autocomplete="off">
                        <div class="position-relative">
                            <div class="card p-2 text-center">
                                <div class="row">
                                    <div class="alert alert-success print-success-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                    <div class="alert alert-danger print-error-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                </div>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close">X</button>
                                <h6>Please enter the one time password <br> to verify your account</h6>
                                <div>
                                    <span>A code has been sent to</span>
                                    <small id="code_sent_user_email">*******9897</small>
                                </div>
                                <div class="digit-group">
                                    <input class="otp" onkeyup="tabChange(1)" type="text" id="digit-1" name="OTP[]" />
                                    <input class="otp" onkeyup="tabChange(2)" type="text" id="digit-2" name="OTP[]" />
                                    <input class="otp" onkeyup="tabChange(3)" type="text" id="digit-3" name="OTP[]" />
                                    <input class="otp" onkeyup="tabChange(4)" type="text" id="digit-4" name="OTP[]" />
                                </div>
                                <span class="err error-sign" id="otp_error"></span>
                                <!-- <div class="mt-4">
                                <button type="button" id="otp_validate_btn" class="btn btn-danger px-4 validate">Validate</button>
                            </div> -->
                                <div class="mt-4 mb-4">
                                    <!-- <button class="btn btn-danger px-4 validate ">Validate</button>  -->
                                    <button type="button" id="otp_validate_btn"
                                        class="btn btn-primary px-4 validate view-report-btn font-semi-bold">Validate</button>
                                </div>
                                <input type="hidden" name="EMAILADDRESS" id="email_address_validate_otp" value="">
                                <input type="hidden" name="PHONENUMBER" id="mobile_number_validate_otp" value="">
                                <input type="hidden" name="_token" id="token_validate_otp"
                                    value="vZWAu33NBWwhpop4yuJApHVSQqcyBlZ6H9vghL4i">
                                <div class="content d-flex justify-content-center align-items-center">
                                    <span>Didn't get the code?</span>
                                    <a id="resend_otp" class="btn-link ms-3 resend-text">Resend</a>
                                </div>
                                <img id="loader_resend" src="public/frontend-assets/images/loader.gif">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- </div> -->


        <div id="res" class="results hideResults results_php">
            <div class="callout" data-closable>
                <button class="close-button" aria-label="Close alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row" id="CompanyRowInput">

            </div>
        </div>



        <script>
        var companyData = [];
        var resultObject = [];
        let results = document.getElementById("res");


        function myFunc(value) {
            if (value !== "") {
                results.classList.remove("hideResults");
            } else {
                results.classList.add("hideResults");
            }

        }






        $(document).ready(function() {

            $(".close-button").on('click', function() {
                $(".results").addClass("hideResults");
                $("#CompanyInput").val('');
            });

            // alert("Hiiiii");



            //    CommentrySectiondata();
            $("#CompanyInput").keyup(function() {


                var cinput = $("#CompanyInput").val();
                if (cinput.length >= 3) {
                    console.log(cinput);
                    headerSearchSectiondata(cinput);
                    // return true;
                }

            });

            $("#CompanyInputmobile").keyup(function() {


                var cinput = $("#CompanyInputmobile").val();
                if (cinput.length >= 4) {
                    console.log(cinput);
                    headerSearchSectiondata(cinput);
                    // return true;
                }
            });





            function headerSearchSectiondata(cinput) {
                var ajaxurl = "header/searchlist.html";
                var ctrinput = $('#country').val();
                $.ajax({
                    type: "get",
                    url: ajaxurl,
                    data: {
                        cinput: cinput,
                        ctrinput: ctrinput
                    },
                    dataType: "JSON"
                }).done(function(response) {
                    if (response) {

                        // console.log(response.data);


                        companyData = response.data;
                        //             var foundValue = companyData.filter(obj=>obj.CompanyName==='IDBI INFO PVT LTD');

                        // console.log(foundValue);
                        //  resultObject = search("IDBI", companyData);
                        //  let obj = companyData.find(x => x.CompanyName === 'IDBI');
                        // console.log(companyData.CompanyName);
                        //  $('#countryList').fadeIn();  
                        $("#CompanyRowInput").html(`
    
 
            <div class="col-lg-6">
                <div class="organization-box">
                    <h3 class="heading-2 text-black mb-4 font-regular">Organization</h3>
                    <ul>
                        ${response.data.map(function(data, index){
  return(`
                    
                        <li><a href="searche099.html?Id=${data.CompanyID}">${data.CompanyName}</a></li>
                         
  `)
  }).join('')}
                    </ul>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="reports-search-box">
                    <h3 class="heading-2 text-black mb-4 font-regular">Reports</h3>
                    <ul>
                        ${response.report.map(function(reportdata, index){
  return(`



                        
                        <li><a href="uploads/newsfiles/%24%7breportdata.html">
                                <p>${reportdata.title}</p>
                                <!--<span class="date-report">7 October ,2022</span>-->
                            </a>
                        </li>
                        
                       


                        `)
  }).join('')}
                    </ul>
                </div>
            </div>
        
   
      
  





  
  `);






                    } else {
                        $("#renderHtmlSearchdata").html("<div>No data Found.....</div>");
                    }

                });
            }
            //Form Submission
            $("#loader").css('display', 'none');
            var $selector = $("#get_otp_login_form");
            form_get_otp2 = $selector.parsley();
            validation = true;
            $("#get_otp_login_btn").click(function(event) {
                event.preventDefault();
                form_get_otp2.validate();
            });
            form_get_otp2.subscribe('parsley:form:success', function(event) {
                $("#loader").css('display', 'block');
                data = $("#get_otp_login_form").serialize();
                $.ajax({
                    url: "https://www.careratingsafrica.com/customergetotp",
                    method: "post",
                    data: data,
                    success: function(response) {
                        if ($.isEmptyObject(response.error)) {
                            $('#get_certificate').css('display', 'block');
                            $("#loader").css('display', 'none');
                            $(".print-success-msg").find("ul").html('');
                            $(".print-success-msg").css('display', 'none');
                            $(".print-error-msgg").find("ul").html('');
                            $(".print-error-msgg").css("display", "none");
                            $(".print-reg-success-msg").find("ul").html('');
                            $(".print-reg-success-msg").css('display', 'none');
                            $(".print-reg-error-msg").find("ul").html('');
                            $(".print-reg-error-msg").css('display', 'none');
                            $(".print-success-msgg").find("ul").html('');
                            $(".print-success-msgg").css('display', 'block');
                            $(".print-success-msgg").find("ul").append('<li>' + response
                                .success + '</li>');
                            //pass to dashboard
                            $('#exampleModalToggle').modal('toggle');
                            $('#exampleModalToggle2').modal('show');
                            if (response.email_address != '') {
                                $("#email_address_validate_otp").val(response
                                .email_address);
                                $("#code_sent_user_email").text(response.email_address);
                            }
                            if (response.PHONENUMBER != '') {
                                $("#mobile_number_validate_otp").val(response.PHONENUMBER);
                            }
                            // window.location.href = "https://www.careratingsafrica.com/user/dashboard";
                            //end
                        } else {
                            printErrorMsgg(response.error);
                        }
                    },
                    error: function(response) {
                        printErrorMsgg(response.error);
                    }
                });
            });
            /*
              Validate form
            */
            var $selector = $("#otp_validate_form");
            // form_validate_otp = $selector.parsley();
            $("#otp_validate_btn").click(function(event) {
                event.preventDefault();
                var validation = true;
                $('#otp_error').text('');
                if ($('.otp').val() == '') {
                    $('#otp_error').text('Please Enter Otp');
                    validation = false;
                };
                if (validation) {
                    data = $("#otp_validate_form").serialize();
                    $.ajax({
                        url: "https://www.careratingsafrica.com/customervalidateotp",
                        method: "post",
                        data: data,
                        success: function(response) {
                            if ($.isEmptyObject(response.error)) {
                                $(".print-success-msg").find("ul").html('');
                                $(".print-success-msg").css('display', 'none');
                                $(".print-error-msgg").find("ul").html('');
                                $(".print-error-msgg").css("display", "none");
                                $(".print-reg-success-msg").find("ul").html('');
                                $(".print-reg-success-msg").css('display', 'none');
                                $(".print-reg-error-msg").find("ul").html('');
                                $(".print-reg-error-msg").css('display', 'none');
                                $(".print-success-msgg").find("ul").html('');
                                $(".print-success-msgg").css('display', 'block');
                                $(".print-success-msgg").find("ul").append('<li>' + response
                                    .success + '</li>');
                                //pass to dashboard
                                window.location.href = "index.html";
                                //end
                            } else {
                                printErrorMsgg(response.error);
                            }
                        },
                        error: function(response) {
                            printErrorMsgg(response.error);
                        }
                    });
                } else {
                    return false;
                }
            });
            /*
              Resend the otp
            */
            $("#loader_resend").css('display', 'none');
            $('#resend_otp').click(function() {
                $("#loader_resend").css('display', 'block');
                data = $("#otp_validate_form").serialize();
                $.ajax({
                    url: "https://www.careratingsafrica.com/customergetotp",
                    method: "post",
                    data: data,
                    success: function(response) {
                        if ($.isEmptyObject(response.error)) {
                            $("#loader_resend").css('display', 'none');
                            $(".print-success-msg").find("ul").html('');
                            $(".print-success-msg").css('display', 'none');
                            $(".print-error-msgg").find("ul").html('');
                            $(".print-error-msgg").css("display", "none");
                            $(".print-reg-success-msg").find("ul").html('');
                            $(".print-reg-success-msg").css('display', 'none');
                            $(".print-reg-error-msg").find("ul").html('');
                            $(".print-reg-error-msg").css('display', 'none');
                            $(".print-success-msgg").find("ul").html('');
                            $(".print-success-msgg").css('display', 'block');
                            $(".print-success-msgg").find("ul").append('<li>' + response
                                .success + '</li>');
                            //pass to dashboard

                            //end
                        } else {
                            printErrorMsgg(response.error);
                        }
                    },
                    error: function(response) {
                        printErrorMsgg(response.error);
                    }
                });
            })
        });

        function printErrorMsgg(msg) {
            $(".print-success-msgg").find("ul").html('');
            $(".print-success-msgg").css('display', 'none');
            $(".print-success-msg").find("ul").html('');
            $(".print-success-msg").css('display', 'none');
            $(".print-reg-success-msg").find("ul").html('');
            $(".print-reg-success-msg").css('display', 'none');
            $(".print-reg-error-msg").find("ul").html('');
            $(".print-reg-error-msg").css('display', 'none');
            $(".print-error-msgg").find("ul").html('');
            $(".print-error-msgg").css('display', 'block');
            $.each(msg, function(key, value) {
                $(".print-error-msgg").find("ul").append('<li>' + value + '</li>');
            });
        }

        let tabChange = function(val) {
            let ele = document.querySelectorAll(".digit-group input");
            if (ele[val - 1].value != "") {
                ele[val].focus();
            } else if (ele[val - 1].value == "") {
                ele[val - 2].focus();
            }
        };
        </script>
        <main class="get-rated-page">
            <section class="inner-banner bg-secondary jumbotron ">
                <div class="container-fluid py-5">
                    <div class="row justify-content-center">
                        <div class="col-md-11">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-5">
                                    <li class="breadcrumb-item">
                                        <a href="index-2.html">Home</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="index-2.html">FIND RATINGS </a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="index-2.html">LATEST RATING </a>
                                    </li>

                                </ol>
                            </nav>

                        </div>
                    </div>
                </div>
            </section>
            <section class="register-form-sec">
                <div class="container-fluid">
                    <div class="latest-area">
                        <div class="latest-Inn">
                            <form action="https://www.careratingsafrica.com/rating-page" method="get">
                                <select name="industry">
                                    <option value="">Select Sector</option>
                                    <option value="Banks">Banks</option>
                                    <option value="Beverages">Beverages</option>
                                    <option value="Ceramics &amp; Sanitaryware">Ceramics &amp; Sanitaryware</option>
                                    <option value="Holding Company">Holding Company</option>
                                    <option value="Hospitality">Hospitality</option>
                                    <option value="Hotels &amp; Hospitality">Hotels &amp; Hospitality</option>
                                    <option value="NBFI">NBFI</option>
                                    <option value="Not Define">Not Define</option>
                                    <option value="Real Estate">Real Estate</option>
                                    <option value="Retail">Retail</option>
                                    <option value="Sugar">Sugar</option>
                                </select>

                                <select name="ratings">
                                    <option value="">Select Rating</option>
                                    <option value="CARE MAU A (SO); Stable">CARE MAU A (SO); Stable</option>
                                    <option value="CARE MAU A (SO); Stable / CARE MAU A1 (SO)">CARE MAU A (SO); Stable /
                                        CARE MAU A1 (SO)</option>
                                    <option value="CARE MAU A (SO); Stable / CARE MAU A1(SO)">CARE MAU A (SO); Stable /
                                        CARE MAU A1(SO)</option>
                                    <option value="CARE MAU A- (SO); Stable">CARE MAU A- (SO); Stable</option>
                                    <option value="CARE MAU A- (SO); Stable /CARE MAU BBB+; Stable">CARE MAU A- (SO);
                                        Stable /CARE MAU BBB+; Stable</option>
                                    <option value="CARE MAU A-; CWD">CARE MAU A-; CWD</option>
                                    <option value="CARE MAU A-; Stable">CARE MAU A-; Stable</option>
                                    <option value="CARE MAU A-; Stable/ CARE MAU A2+">CARE MAU A-; Stable/ CARE MAU A2+
                                    </option>
                                    <option value="CARE MAU A-;Stable">CARE MAU A-;Stable</option>
                                    <option value="CARE MAU A; Positive">CARE MAU A; Positive</option>
                                    <option value="CARE MAU A; Stable">CARE MAU A; Stable</option>
                                    <option value="CARE MAU A; Stable / CARE MAU A1">CARE MAU A; Stable / CARE MAU A1
                                    </option>
                                    <option value="CARE MAU A; Stable / CARE MAU BBB+; stable">CARE MAU A; Stable / CARE
                                        MAU BBB+; stable</option>
                                    <option value="CARE MAU A; Stable/ CARE MAU A-; Stable">CARE MAU A; Stable/ CARE MAU
                                        A-; Stable</option>
                                    <option value="CARE MAU A+ (SO); Stable">CARE MAU A+ (SO); Stable</option>
                                    <option value="CARE MAU A+ (SO); Stable / CARE MAU A1 (SO)">CARE MAU A+ (SO); Stable
                                        / CARE MAU A1 (SO)</option>
                                    <option value="CARE MAU A+; Stable">CARE MAU A+; Stable</option>
                                    <option value="CARE MAU A+; Stable / CARE MAU A1">CARE MAU A+; Stable / CARE MAU A1
                                    </option>
                                    <option value="CARE MAU A+; Stable /Withdrawn">CARE MAU A+; Stable /Withdrawn
                                    </option>
                                    <option value="CARE MAU A+; Stable/ CARE MAU A1+">CARE MAU A+; Stable/ CARE MAU A1+
                                    </option>
                                    <option value="CARE MAU A1">CARE MAU A1</option>
                                    <option value="CARE MAU A1 (SO)">CARE MAU A1 (SO)</option>
                                    <option value="CARE MAU A1+">CARE MAU A1+</option>
                                    <option value="CARE MAU A2">CARE MAU A2</option>
                                    <option value="CARE MAU A2+">CARE MAU A2+</option>
                                    <option value="CARE MAU A2+/ CARE MAU A-; Stable">CARE MAU A2+/ CARE MAU A-; Stable
                                    </option>
                                    <option value="CARE MAU AA (SO);Stable">CARE MAU AA (SO);Stable</option>
                                    <option value="CARE MAU AA- (SO);Stable">CARE MAU AA- (SO);Stable</option>
                                    <option value="CARE MAU AA-; Positive">CARE MAU AA-; Positive</option>
                                    <option value="CARE MAU AA-; Stable">CARE MAU AA-; Stable</option>
                                    <option value="CARE MAU AA-; Stable/ CARE MAU A1+">CARE MAU AA-; Stable/ CARE MAU
                                        A1+</option>
                                    <option value="CARE MAU AA-;Stable">CARE MAU AA-;Stable</option>
                                    <option value="CARE MAU AA; Positive">CARE MAU AA; Positive</option>
                                    <option value="CARE MAU AA; Positive/ CARE MAU A1+">CARE MAU AA; Positive/ CARE MAU
                                        A1+</option>
                                    <option value="CARE MAU AA; Stable">CARE MAU AA; Stable</option>
                                    <option value="CARE MAU AA; Stable/ CARE MAU A1+">CARE MAU AA; Stable/ CARE MAU A1+
                                    </option>
                                    <option value="CARE MAU AA+ (IS); Stable">CARE MAU AA+ (IS); Stable</option>
                                    <option value="CARE MAU AAA (Is); Stable">CARE MAU AAA (Is); Stable</option>
                                    <option value="CARE MAU AAA; Stable">CARE MAU AAA; Stable</option>
                                    <option value="CARE MAU BBB; Stable">CARE MAU BBB; Stable</option>
                                    <option value="CARE MAU BBB+; CWD">CARE MAU BBB+; CWD</option>
                                    <option value="CARE MAU BBB+; Stable">CARE MAU BBB+; Stable</option>
                                    <option value="Withdrawn">Withdrawn</option>
                                </select>

                                <select name="countrynames">
                                    <option value="">Select Country</option>
                                    <option value="Mauritius">Mauritius</option>
                                </select>
                                <button class="btn btn-primary btn-default">Search</button>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Company</th>
                                            <th>Rating</th>
                                            <th>Latest Review Date</th>
                                            <th>Country</th>
                                            <th>Sector</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="searchc90b.html?Id=xAOOnQB3BfE9QYWzbTVqeg==">Alteo Limited</a>
                                            </td>
                                            <td>CARE MAU A (SO); Stable</td>
                                            <td> May 11, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Sugar</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search6e55.html?Id=Ndf6D35i0E9eSCFDYlQ6NQ==">Anahita Hotel
                                                    Limited</a></td>
                                            <td>CARE MAU A; Stable</td>
                                            <td> Dec 21, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Hospitality</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searchc117.html?Id=wfsKJ+j1l9LKaT82S/t9rQ==">Ascencia
                                                    Limited</a></td>
                                            <td>CARE MAU AA-;Stable</td>
                                            <td> Nov 9, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Real Estate</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searche6e9.html?Id=CO2nmgKE5y8deHlzDfgfiw==">Attitude
                                                    Hospitality Ltd</a></td>
                                            <td>CARE MAU A-; Stable</td>
                                            <td> Mar 14, 2024</td>
                                            <td>Mauritius</td>
                                            <td>Hotels &amp; Hospitality</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searchb7f1.html?Id=PZ86TN0+jCiYLvRzlR03lw==">Attitude Property
                                                    Ltd</a></td>
                                            <td>CARE MAU A-; Stable</td>
                                            <td> Mar 19, 2024</td>
                                            <td>Mauritius</td>
                                            <td>Hospitality</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searcha10d.html?Id=K64xYSBtQkTMFLvHkxRHSA==">Axess Limited</a>
                                            </td>
                                            <td>CARE MAU A+; Stable/ CARE MAU A1+</td>
                                            <td> Dec 20, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Not Define</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searchd308.html?Id=GEMxHrMFNdMwtmohXxkPvA==">Bank One
                                                    Limited</a></td>
                                            <td>CARE MAU A+; Stable</td>
                                            <td> Jun 29, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Banks</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search4f94.html?Id=p4Hrkv3OFMDVBrzov4VaLg==">BH Property
                                                    Investments Limited</a></td>
                                            <td>CARE MAU A; Positive</td>
                                            <td> Aug 4, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Real Estate</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search6110.html?Id=mQP94IMTL6pQyJrhTGU29Q==">C-Care (Mauritius)
                                                    Ltd</a></td>
                                            <td>CARE MAU AA-; Stable/ CARE MAU A1+</td>
                                            <td> Nov 30, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Hospitality</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search7ea8.html?Id=416t2GnDK02Am2AYy0XDMA==">Chartreuse Group
                                                    Ltd</a></td>
                                            <td>CARE MAU BBB; Stable</td>
                                            <td> Jun 10, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Not Define</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search18d2.html?Id=Kp1HFQyRMtQwKY2OBZYpyQ==">CIEL Finance
                                                    Limited</a></td>
                                            <td>CARE MAU A; Stable</td>
                                            <td> Nov 30, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Banks</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searchf75f.html?Id=zhHKODvZuMHt/vN6Xc5VZQ==">CIEL Limited</a>
                                            </td>
                                            <td>CARE MAU AA; Stable/ CARE MAU A1+</td>
                                            <td> Nov 30, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Banks</td>
                                        </tr>
                                        <tr>
                                            <td><a href="searchbc19.html?Id=ZRr1eOreaL97AAuksfeFvQ==">CIM Financial
                                                    Services Ltd</a></td>
                                            <td>CARE MAU AA; Positive/ CARE MAU A1+</td>
                                            <td> Feb 20, 2024</td>
                                            <td>Mauritius</td>
                                            <td>NBFI</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search6edf.html?Id=PXUt0kwHIXbKs3wvwtTM3A==">City and Beach
                                                    Hotels (Mauritius) Limited</a></td>
                                            <td>CARE MAU A; Stable</td>
                                            <td> Sep 29, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Hotels &amp; Hospitality</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search2c3a.html?Id=USJSL1DQkWL4cJJX1dPdQA==">CM Diversified
                                                    Credit Ltd</a></td>
                                            <td>CARE MAU A (SO); Stable / CARE MAU A1 (SO)</td>
                                            <td> Dec 20, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Ceramics &amp; Sanitaryware</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search93bc.html?Id=ToXmumzqPpEsfD0+I4vACw==">CM Structured
                                                    Finance (1) Ltd</a></td>
                                            <td>Withdrawn</td>
                                            <td> Feb 16, 2024</td>
                                            <td>Mauritius</td>
                                            <td>NBFI</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search53f3.html?Id=199Hye2h8uvhKmLormcIIA==">CM Structured
                                                    Finance (2) Ltd</a></td>
                                            <td>CARE MAU A (SO); Stable / CARE MAU A1 (SO)</td>
                                            <td> Nov 9, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Ceramics &amp; Sanitaryware</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search8f83.html?Id=OEIcgKK0yPZ2/DDSRbXrHQ==">CM Structured
                                                    Products (1) Ltd</a></td>
                                            <td>CARE MAU A (SO); Stable</td>
                                            <td> Aug 7, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Not Define</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search83be.html?Id=orVhxFIpMgeQIQum1QC8EQ==">CM Structured
                                                    Products (2) Ltd</a></td>
                                            <td>CARE MAU A (SO); Stable / CARE MAU A1 (SO)</td>
                                            <td> Nov 9, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Ceramics &amp; Sanitaryware</td>
                                        </tr>
                                        <tr>
                                            <td><a href="search2f78.html?Id=/thGg/MRzJFwcvNv61YAqA==">Commercial
                                                    Investment Property Fund Limited</a></td>
                                            <td>CARE MAU A-; Stable</td>
                                            <td> Nov 7, 2023</td>
                                            <td>Mauritius</td>
                                            <td>Not Define</td>
                                        </tr>
                                    </tbody>

                                </table>



                            </div>

                        </div>

                    </div>
                </div>
                <div class="d-flex justify-content-center pagina">
                    <nav role="navigation" aria-label="Pagination Navigation" class="flex items-center justify-between">
                        <div class="flex justify-between flex-1 sm:hidden">
                            <span
                                class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                                &laquo; Previous
                            </span>

                            <a href="rating-page4658.html?page=2"
                                class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                                Next &raquo;
                            </a>
                        </div>

                        <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                            <div>
                                <p class="text-sm text-gray-700 leading-5">
                                    Showing
                                    <span class="font-medium">1</span>
                                    to
                                    <span class="font-medium">20</span>
                                    of
                                    <span class="font-medium">71</span>
                                    results
                                </p>
                            </div>

                            <div>
                                <span class="relative z-0 inline-flex shadow-sm rounded-md">

                                    <span aria-disabled="true" aria-label="&amp;laquo; Previous">
                                        <span
                                            class="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-l-md leading-5"
                                            aria-hidden="true">
                                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                                <path fill-rule="evenodd"
                                                    d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                                    clip-rule="evenodd" />
                                            </svg>
                                        </span>
                                    </span>





                                    <span aria-current="page">
                                        <span
                                            class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5">1</span>
                                    </span>
                                    <a href="rating-page4658.html?page=2"
                                        class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                        aria-label="Go to page 2">
                                        2
                                    </a>
                                    <a href="rating-page9ba9.html?page=3"
                                        class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                        aria-label="Go to page 3">
                                        3
                                    </a>
                                    <a href="rating-pagefdb0.html?page=4"
                                        class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                                        aria-label="Go to page 4">
                                        4
                                    </a>


                                    <a href="rating-page4658.html?page=2" rel="next"
                                        class="relative inline-flex items-center px-2 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-r-md leading-5 hover:text-gray-400 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150"
                                        aria-label="Next &amp;raquo;">
                                        <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                            <path fill-rule="evenodd"
                                                d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </nav>

                </div>
            </section>

            <?php include "components/footer.php" ?>