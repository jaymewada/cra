<?php include 'components/header.php' ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlStartedSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Publications</li>
                        <li class="breadcrumb-item active" aria-current="page">Research</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Research</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding-100">

</section>

<?php include "components/footer.php" ?>