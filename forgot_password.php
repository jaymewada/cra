<?php include "components/header.php" ?>

<section class="sitemap padding-100">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">

                <div class="card">

                    <div class="card-header">Forgot Password</div>

                    <div class="card-body">

                        <form action="https://www.careratingsafrica.com/submit_forgot_password" method="POST">

                            <input type="hidden" name="_token" value="48o4F2VKAWLAsDh91E2wEqw4IE5EqzJ5W7xzFTXK">
                            <div class="form-group row">

                                <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail
                                    Address</label>

                                <div class="col-md-6">

                                    <input type="text" id="email_address" class="form-control" name="email" required=""
                                        autofocus="" jf-ext-cache-id="7">
                                </div>
                            </div>
                            <br>
                            <div class="col-md-6 offset-md-4">

                                <button type="submit" class="btn btn-primary" style="margin-left: 10px;"
                                    jf-ext-button-ct="send password reset link">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6"></div>
    </div>

</section>

<?php include "components/footer.php" ?>