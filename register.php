<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Register</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Register</h1>
                    <div class="w-auto hav-acc-div d-flex justify-content-md-between align-items-xl-center">
                        <span>Already have an account ?</span> <a data-bs-toggle="modal" href="#exampleModalToggle"
                            class="btn btn-primary ms-lg-4">LOGIN
                            HERE</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="register-form-sec">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-11 col-sm-12 col-md-12 col-lg-12 col-xl-9 text-center p-0 mt-3 mb-2">
                <div class="card1 px-0 pt-4 pb-0 mt-3 mb-3">
                    <div id="msform">
                        <!-- progressbar -->
                        <ul id="progressbar">
                            <li class="active" id="account"><strong>Personal Details</strong></li>
                            <li id="payment"><strong>Company Details</strong></li>
                            <li id="personal"><strong>Login Details</strong></li>
                        </ul>
                        <div class="row">
                            <div class="alert alert-success print-reg-success-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <div class="alert alert-danger print-reg-error-msg" style="display:none">
                                <ul></ul>
                            </div>
                        </div>
                        <form id="form1" name="form1" novalidate="">
                            <div class="form-card">
                                <div class="contact-form">
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" data-parsley-pattern="/^[a-zA-Z\s]+$/"
                                                name="First_Name" id="First_Name" class="form-control"
                                                placeholder="firstname" required="" data-parsley-trigger="focusout"
                                                data-parsley-pattern-message="First Name should not include special character and number"
                                                data-parsley-required-message="Enter Your First Name"
                                                data-parsley-minlength="3"
                                                data-parsley-minlength-message="First Name must be at least 3 characters"
                                                jf-ext-cache-id="7">
                                            <label for="First_Name">First Name<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Last_Name" id="Last_Name" required=""
                                                class="form-control" placeholder="lastname"
                                                data-parsley-trigger="focusout"
                                                data-parsley-pattern-message="Last Name should not include special character and number"
                                                data-parsley-required-message="Enter Your Last Name"
                                                data-parsley-minlength="3"
                                                data-parsley-minlength-message="Last Name must be at least 3 characters"
                                                jf-ext-cache-id="8">
                                            <label for="Last_Name">Last Name<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Mobile_Number" id="Mobile_Number"
                                                required="" class="form-control" placeholder="+91 000 000 0000"
                                                maxlength="10" data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Mobile Number"
                                                data-parsley-email-message="Enter a valid Mobile Number"
                                                jf-ext-cache-id="9">
                                            <label for="floatingInput">Mobile No<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <!--<div class="col-md-12">
                                            <div class="form-floating">
                                                <input type="text" value="" name="Contact_Number" id="Contact_Number" class="form-control" placeholder="+91 000 000 0000" data-parsley-trigger="focusout" data-parsley-required-message="Enter Your Contact Number">
                                                <label for="Contact_Number">Contact No</label>
                                            </div>
                                        </div>-->
                                </div>
                            </div>
                            <input type="button" name="next" id="btn_next_1" class="action-button" value="PROCEED">
                        </form>
                        <form id="form2" name="form2" novalidate="">
                            <div class="form-card">
                                <div class="contact-form">
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Organization_Name" id="Organization_Name"
                                                class="form-control" placeholder="Organization Name" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Organization Name"
                                                jf-ext-cache-id="10">
                                            <label for="floatingInput">Organization Name<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Designation" id="Designation"
                                                class="form-control" placeholder="Designation" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Designation"
                                                jf-ext-cache-id="11">
                                            <label for="floatingInput">Designation<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <textarea type="text" value="" name="Address" id="Address"
                                                class="form-control" placeholder="Address" maxlength="300" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Address"
                                                jf-ext-cache-id="20"></textarea>
                                            <label for="floatingInput">Address<sup>*</sup></label>
                                            <div id="the-count">
                                                <span id="current">0</span>
                                                <span id="maximum">/ 300</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Door_no" id="Door_no" class="form-control"
                                                placeholder="Door/Flat No" required="" data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Door/Flat No"
                                                jf-ext-cache-id="12">
                                            <label for="floatingInput">Door/Flat No.<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Landmark" id="Landmark"
                                                class="form-control" placeholder="Landmark"
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Landmark"
                                                jf-ext-cache-id="13">
                                            <label for="floatingInput">Landmark (Optional)</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="text" value="" name="Zip_Code" id="Zip_Code"
                                                class="form-control" placeholder="Zip Code" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Zip Code"
                                                jf-ext-cache-id="14">
                                            <label for="floatingInput">Zip Code<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <!-- <input type="text" class="form-control" placeholder="Country"> -->
                                            <select name="Country" id="Country" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Country"
                                                onchange="change_country(this.value, 'State');" jf-ext-cache-id="21">
                                                <option value="">Country</option>
                                                <option value="1">Mauritius</option>
                                                <option value="2">India</option>
                                                <option value="3">Sri Lanka</option>
                                                <option value="4">Africa</option>
                                                <option value="5">South Africa</option>
                                            </select>
                                            <!-- <label for="floatingInput">Country<span>*</span></label> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <!-- <input type="text" class="form-control" placeholder="City"> -->
                                            <select name="State" id="State" required="" data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your State"
                                                onchange="change_state(this.value, 'City');" jf-ext-cache-id="22">
                                                <option value="">State</option>
                                            </select>
                                            <!-- <label for="floatingInput">City<span>*</span></label> -->
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- <div class="form-floating autocomplete" id="city_dropdown"> -->
                                        <div class="form-floating autocomplete" id="city_dropdown">

                                            <input type="text" class="form-control" placeholder="City" name="City"
                                                id="City" required="" data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your City" jf-ext-cache-id="15">
                                            <label for="floatingInput">City<span>*</span></label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <input type="button" name="next" id="btn_next_2" class=" action-button" value="PROCEED">
                            <input type="button" name="previous" id="btn_back_2" class=" action-button-previous"
                                value="Back">
                        </form>
                        <form id="form3" name="form3" novalidate="">
                            <div class="form-card login-otp text-center">
                                <div class="contact-form">
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input type="email" value="" name="Email_ID" id="Email_ID"
                                                class="form-control" placeholder="email" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Email" jf-ext-cache-id="16">
                                            <label for="Email_ID">Email ID<sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input id="password-field_register" type="password" class="form-control"
                                                name="Password" placeholder="password" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Password">
                                            <span toggle="#password-field_register"
                                                class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            <label for="floatingInput">Password*</label>

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-floating">
                                            <input id="password-field1" type="password"
                                                data-parsley-equalto="#password-field_register" class="form-control"
                                                name="confirmpassword" placeholder="Confirm password" required=""
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Your Confirm password">
                                            <span toggle="#password-field1"
                                                class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            <label for="floatingInput">Confirm Password*</label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="_token" id="token_user_register"
                                value="48o4F2VKAWLAsDh91E2wEqw4IE5EqzJ5W7xzFTXK">
                            <input type="button" name="next" id="btn_next_3" class=" action-button" value="PROCEED">
                            <input type="button" name="previous" id="btn_back_3" class=" action-button-previous"
                                value="Back">
                            <img id="loader_reg"
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/loader.gif"
                                style="display: none;">

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>



</section>
<?php include "components/footer.php" ?>