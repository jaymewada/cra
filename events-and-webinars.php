<?php include "components/header.php" ?>

<section class="webinars1-sec webinars1_sec_php padding-100 yearDropDown-main arrow-relative-parent">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <div>
                        <h3 class="heading-1 text-black">Webinars</h3>
                    </div>
                    <div
                        class="mt-xl-3 d-flex custom-dropdown filter-dropdown  dropdown-menu-outiline  dropdown mb-5  mb-lg-0 ">
                        <div class="row justify-content-center">
                            <div class="slider-arrows">

                                <span class="prev-btn ">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-left" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                        </path>
                                    </svg>
                                </span>
                                <span class="next-btn">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                        </path>
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex sasifb flex-row-reverse ">
                            <div class="yearToggle" style="margin-left:5px;" id="renderHtmlMediaYeardata">
                                <select class="empInput form-control" name="Year_Id" id="Webinar_Year_Id"
                                    style="border: 1px solid #858796;" jf-ext-cache-id="10">
                                    <option value="2023" year-data="2023">2023</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="webinars1-slider  left-space-sldier verflow-hidden mb-2 ">
            <?php for ($x = 0; $x <= 10; $x++) { ?>
            <div>
                <div class="item latest">
                    <div class=" card card-style-2 ">

                        <div class=" card-image black-overlay position-relative d-flex justify-content-center
                    align-items-center">
                            <img src="public/frontend-assets/images/webinar-slider.jpeg" class="card-img-top img-fluid"
                                alt="...">
                            <div class="vide_playbtn small animayion-none">

                                <a href="https://www.youtube.com/embed/iIyqhBA92do" target="_blank" class="play__btn"
                                    tabindex="0">
                                    <span></span>
                                </a>
                            </div>
                        </div>
                        <div class="card-body border-0 p-0">
                            <p class="text-primary mb-2 mt-3 font-semi-bold">Deciphering Data Centre Ecosystem</p>
                            <h3 class="heading-2">
                                <p>March 15, 2024</p>
                            </h3>
                        </div>

                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>

<section class="CareEdgeeventgallery-sec padding-100 eventGallery-modal yearDropDown-main">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <div>
                        <h3 class="heading-1 text-black">Events Gallery</h3>
                    </div>

                    <div class="d-flex custom-dropdown filter-dropdown dropdown-menu-outiline dropdown mb-5 mb-lg-0 ">
                        <div class="row justify-content-center">
                            <div class="slider-arrows">
                                <span class="prev-btn slick-arrow slick-disabled" style="" aria-disabled="true">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-left" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                        </path>
                                    </svg>
                                </span>
                                <span class="next-btn slick-arrow" style="" aria-disabled="false">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                        </path>
                                    </svg>
                                </span>
                            </div>
                        </div>

                        <div class="d-flex sasifb flex-row-reverse ">

                            <div class="yearToggle" style="margin-left:5px;" id="renderHtmlEventsGalleryYeardata">
                                <select class="empInput form-control" name="Year_Id" id="eventgalYear_Id"
                                    style="border: 1px solid #858796;" jf-ext-cache-id="11">
                                    <option value="2021" id="2021">2021</option>
                                    <option value="2020" id="2020">2020</option>
                                    <option value="1970" id="1970">1970</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="renderHtmlHomeEventsGallerySectiondata">
        <div class="eventgallery-slider left-space-sldier overflow-hidden mb-2 pt-0">
            <?php for ($x = 0; $x <= 10; $x++) { ?>
            <div>
                <div class="item latest">
                    <div class=" card card-style-2">
                        <a class="btn trigger openModal_php" data-bs-toggle="modal" href="#exampleModalToggle-e1"
                            data-url="public/frontend-assets/images/thumbnail.jpg"
                            data-desc="<p>hosted by The Centre for Energy Finance at the Council on Energy, Environment, and Water (CEEW-CEF) on January 16. The discussion focused on strategies to activate the domestic corporate green bond market to facilitate debt capital recycling for renewable energy.</p>"
                            role="button" tabindex="0">
                            <div class="card-image position-relative">
                                <img src="public/frontend-assets/images/thumbnail.jpg" class="card-img-top img-fluid"
                                    alt="...">
                                <label class="date-lable">16TH JANUARY 2024</label>
                            </div>
                        </a>
                        <div class="card-body border-0 p-0">
                            <h3 class="heading-2 mt-4 mb-3">Rajashree Murkute, Director at CareEdge Ratings, contributed
                                to
                                a panel discussion on 'Unlocking India's Domestic Corporate Green Bond Market
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>

    <!-- Modal popup for desktop-->
    <div class="modal fade login-popup eventGal-popup" id="exampleModalToggle-e1" aria-hidden="true"
        aria-labelledby="exampleModalToggleLabel-e1" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                        jf-ext-button-ct="x">X</button>
                </div>
                <div class="modal-body">
                    <div class="eventGal_modal-content">
                        <img class="eventGal-popup-img" src="" alt="" width="100%">

                        <div class="eventGal_modal_text">
                            <p class="eventGal_modal_text-para">
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>