<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">CAPABILITIES</a></li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">FINANCIAL SECTOR</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">FINANCIAL SECTOR</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 " style="background-color: #F0F0F0; ">
    <div class="container-fluid" id="renderHtmlOverviewCapabilitySectiondata">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-top">
                    <div class="col-md-12 col-xl-12">
                        <h3 class="heading-1 text-black ">Overview</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <p>CRAF rates debt instruments/ bank facilities of a large number of companies in the financial
                            sector, including banks, non-banking finance companies, housing finance companies, financial
                            institutions etc.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="capabilities-sec bg-grey overflow-hidden"
    style="background-image:url(https://www.careratingsafrica.com/public/frontend-assets/images/Capabilities.png);">
    <div class="container-fluid left-space pe-0" id="renderHtmlHomeCapabilitySectorSectiondata">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading-1 text-black">Financial Sector</h3>
            </div>
        </div>
        <div class="accordian-style-1 mt-5 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center">Banks/NBFCs/FIs</span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">


                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"
                                jf-ext-button-ct="banks/nbfcs/fis">
                                Banks/NBFCs/FIs
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">

                            <div class="accordion-body">
                                <p></p>
                                <p class="heading-3 para-line-height">CRAF rates debt instruments/ bank facilities of a
                                    large number of companies in the financial sector, including banks, non-banking
                                    finance companies, housing finance companies, financial institutions etc.
                                    Rating determination is a matter of experienced and holistic judgement, based on the
                                    relevant quantitative and qualitative factors affecting the credit quality of the
                                    issuer. The analytical framework of CRAF’s rating methodology is divided into two
                                    interdependent segments- The first deals with the operational characteristics and
                                    the second with the financial characteristics. Besides quantitative factors,
                                    qualitative aspects like an assessment of management capabilities play a very
                                    important role in arriving at the rating for an instrument. The relative importance
                                    of qualitative and quantitative components of the analysis varies with the type of
                                    issuer. CRAF largely uses the ‘CRAMELS’ model to assess entities in the financial
                                    sector; essentially covering Capital adequacy, Resource raising ability, Asset
                                    quality, Management quality, Earnings quality, Liquidity and Systems. Apart from
                                    that, CRAF factors in the nuances of the industry in which the financial sector
                                    entity operates.
                                </p>
                                <p class="heading-3 para-line-height">CRAF’s offerings for the financial sector include:
                                </p>
                                <ul class="fee-structure-number1">
                                    <li>Debt Instrument ratings</li>
                                    <li>
                                        Long-term instruments like
                                        <ul>
                                            <li>Bonds (Lower Tier II bonds under Basel II, Upper Tier II bonds under
                                                Basel II, Tier I – Innovative Perpetual Debt Instruments under Basel II,
                                                Tier II Bonds under Basel III, Tier I bonds under Basel III)</li>
                                            <li>Non-Convertible Debentures (NCDs)</li>
                                            <li>Principal Protected Market Linked Debentures</li>
                                            <li>Subordinate Debt</li>
                                            <li>Perpetual Bonds</li>
                                            <li>Hybrid Instruments</li>
                                            <li>Preference Shares</li>
                                        </ul>
                                    </li>
                                    <li>
                                        Medium-Term instruments like
                                        <ul>
                                            <li>Fixed Deposits (FDs)</li>
                                        </ul>
                                    </li>
                                    <li>
                                        Short-Term instruments like
                                        <ul>
                                            <li>Money Market Instruments (MMIs), Commercial Papers (CPs) and Short-Term
                                                Debt Programs of NBFCs</li>
                                            <li>CDs of Banks</li>
                                        </ul>
                                    </li>
                                    <li>Bank Loan Ratings</li>
                                </ul>
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId1" class="accordion-button collapsed" type="button"
                                jf-ext-button-ct="insurance sector">
                                Insurance Sector
                            </button>
                        </h2>
                        <div id="collapse1" class="accordion-collapse collapse">

                            <div class="accordion-body">
                                <p></p>
                                <p>CRAF’s Issuer Rating for Insurance sector companies is an opinion on a life or
                                    non-life insurance company’s business and financial strength and measures its
                                    ability to honor policy holders’ obligations and debt payments, if any, as per
                                    contractual commitments. The opinion about policy holders’ obligations is not
                                    specific to any insurance policy or contract.</p><br>
                                <p>The issuer rating assigned to an insurance company is not specific to any particular
                                    insurance policy or contract and does not address the suitability of terms of any
                                    individual policy or contracts. Additionally, the issuer rating neither considers
                                    deductibles, surrender or cancellation penalties nor does it account for likelihood
                                    of the use of a defense like fraud to deny claims. The rating also does not consider
                                    any limitation that the insurers might face in settling its foreign claims due to
                                    exchange control/sovereign restrictions that might be placed on foreign currency
                                    payments by the Government of Mauritius.</p><br>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>