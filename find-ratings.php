<?php include 'components/header.php' ?>

<div id="res" class="resultPan results hideResults results_php">
    <div class="callout" data-closable>
        <button class="close-button" aria-label="Close alert" type="button" data-close> <span
                aria-hidden="true">&times;</span> </button>
    </div>
    <div class="row" id="CompanyRowInput">
        <div class="col-lg-6">
            <div class="organization-box">
                <h3 class="heading-2 text-black mb-4 font-regular">Organization</h3>
                <ul>
                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="reports-search-box">
                <h3 class="heading-2 text-black mb-4 font-regular">Reports</h3>
                <ul>
                    <li><a href="uploads/newsfiles/${reportdata.pdf}">
                            <p>Reports Title</p>
                            <span class="date-report">7 October ,2022</span>
                        </a></li>
                    <li><a href="uploads/newsfiles/${reportdata.pdf}">
                            <p>Reports Title</p>
                            <span class="date-report">7 October ,2022</span>
                        </a></li>
                    <li><a href="uploads/newsfiles/${reportdata.pdf}">
                            <p>Reports Title</p>
                            <span class="date-report">7 October ,2022</span>
                        </a></li>
                    <li><a href="uploads/newsfiles/${reportdata.pdf}">
                            <p>Reports Title</p>
                            <span class="date-report">7 October ,2022</span>
                        </a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="home-slider-sec position-relative">
    <div class="inner-banner">
        <img src="public/frontend-assets/images/find-ratings-banner.jpg" class="img-fluid">
        <div class="banner-content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Find Ratings</li>
                </ol>
            </nav>
            <div class="banner_captions">
                <h1 class="heading-1 text-white">Find Ratings</h1>
                <h3 class="heading-2 text-white font-light">Search for the Company you want to know the ratings of</h3>
            </div>
            <div class="filter-ratings d-xl-block position-relative" id="searchres5">
                <div class="searchBar find-rate-searchbar">
                    <form action="findsearch" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="S6XApwKxGpSsRBQx2ZWNcglnkL3GGpmQcYQkLlLP">
                        <div class="input-find-rating">
                            <select class="form-control"
                                style="background-color: rgb(255 255 255 / 14%);width: 28%;color:#fff;">
                                <option value="Mauritius" selected="">Mauritius</option>
                            </select>
                        </div>
                        <div class="input-find-rating"> <span class="search-icon">
                                <svg id="Icons_Actions_ic-actions-search"
                                    data-name="Icons / Actions / ic-actions-search" xmlns="http://www.w3.org/2000/svg"
                                    width="24" height="24" viewBox="0 0 24 24">
                                    <rect id="Rectangle_157" data-name="Rectangle 157" width="24" height="24"
                                        fill="none"></rect>
                                    <g id="ic-actions-search" transform="translate(3.62 3.55)">
                                        <circle id="Ellipse_12" data-name="Ellipse 12" cx="7" cy="7" r="7"
                                            transform="translate(0)" fill="none" stroke="#fff" stroke-linecap="round"
                                            stroke-linejoin="bevel" stroke-width="1.5"></circle>
                                        <line id="Line_50" data-name="Line 50" x2="4.88" y2="4.88"
                                            transform="translate(11.88 12.02)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.5"></line>
                                    </g>
                                </svg>
                            </span>
                            <input type="hidden" name="_token" id="token_find_ratings"
                                value="S6XApwKxGpSsRBQx2ZWNcglnkL3GGpmQcYQkLlLP">
                            <input type="text" id="CompanyFindRatingInput" oninput="myFunc5(this.value)" value=""
                                placeholder="Enter company name" name="companyName" class="form-control search-input">
                            <button type="submit" name="submit" class="btn btn-primary find-rating">FIND
                                RATINGS</button>
                            <div id="res5" class="resultPan results5 hideResults5">
                                <div class="callout" data-closable>
                                    <button class="close-button" aria-label="Close alert" type="button" data-close>
                                        <span aria-hidden="true">&times;</span> </button>
                                </div>
                                <div class="row" id="CompanyfindRatingRowInput">
                                    <div class="col-lg-6">
                                        <div class="organization-box">
                                            <h3 class="heading-2 text-black mb-4 font-regular">Organization</h3>
                                            <ul>
                                                <li><a href="search?Id=WhefgXy53RogxCBJbjWRbw==">ABC Banking Corporation
                                                        Ltd</a></li>
                                                <li><a href="search?Id=xAOOnQB3BfE9QYWzbTVqeg==">Alteo Limited</a></li>
                                                <li><a href="search?Id=Ndf6D35i0E9eSCFDYlQ6NQ==">Anahita Hotel
                                                        Limited</a></li>
                                                <li><a href="search?Id=k3wc5y4HvXjrZ7S3lvDt8Q==">Aquarelle International
                                                        Limited</a></li>
                                                <li><a href="search?Id=wfsKJ+j1l9LKaT82S/t9rQ==">Ascencia Limited</a>
                                                </li>
                                                <li><a href="search?Id=CO2nmgKE5y8deHlzDfgfiw==">Attitude Hospitality
                                                        Ltd</a></li>
                                                <li><a href="search?Id=PZ86TN0+jCiYLvRzlR03lw==">Attitude Property
                                                        Ltd</a></li>
                                                <li><a href="search?Id=K64xYSBtQkTMFLvHkxRHSA==">Axess Limited</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="reports-search-box">
                                            <h3 class="heading-2 text-black mb-4 font-regular">Press Release</h3>
                                            <ul>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202403100457_Attitude_Property_Ltd__-_Press_Release_29_March_2024.pdf">
                                                        <p>Attitude Property Ltd</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202403110338_Attitude_Hospitality_Ltd__-_Press_Release_28_March_2024.pdf">
                                                        <p>Attitude Hospitality Ltd</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202402100237_Anahita_Hotel_Limited__-_Press_Release_27_February_2024.pdf">
                                                        <p>Anahita Hotel Limited</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202402120212_ABC_Banking_Corporation_Ltd_-_Press_Release_05_February_2024.pdf">
                                                        <p>ABC Banking Corporation Ltd</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202312120156_Axess_Limited_-_Press_Release_21_December_2023.pdf">
                                                        <p>Axess &amp; Co. Ltd</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202311050250_Ascencia_Limited_-_Press_Release_November_2023.pdf">
                                                        <p>Ascencia Limited</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202308071110_Attitude_Hospitality_Ltd__-_Press_Release_15_August_2023.pdf">
                                                        <p>Attitude Hospitality Ltd</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202306060247_Axess_Limited_-_Press_Release_13_June_2023.pdf">
                                                        <p>Axess &amp; Co. Ltd</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202305071143_Alteo_Limited_-__Press_Release_19_May_2023.pdf">
                                                        <p>Alteo Limited</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                                <li><a target="_blank"
                                                        href="upload/CompanyFiles/PR/202304130111_Alteo_Limited_-_Press_Release_19_April_2023.pdf">
                                                        <p>Alteo Limited</p>
                                                        <span class="date-report">7 October ,2022</span>
                                                    </a> </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="recent-ratings-list">
        <h3 class="heading-3">Recent Ratings</h3>
        <div class="scrollbar">
            <ul class="p-0 m-0" data-mcs-theme="light" id="renderHtmlCompanyRecentReportsSection">
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=7KXW6CE7ElcRkr4ygWi0sg==">Miwa
                            Sugar
                            Limited</a> <span class="text-small text-light">3RD APRIL 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=FymTiyNomHF/KZ/JfSMzhw==">MaxCity
                            Property
                            Fund
                            Ltd</a> <span class="text-small text-light">2ND JULY 2021</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=PZ86TN0+jCiYLvRzlR03lw==">Attitude
                            Property
                            Ltd</a> <span class="text-small text-light">29TH MARCH 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=ZRr1eOreaL97AAuksfeFvQ==">CIM
                            Financial
                            Services
                            Ltd</a> <span class="text-small text-light">20TH FEBRUARY 2023</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=p4Hrkv3OFMDVBrzov4VaLg==">BH
                            Property
                            Investments
                            Limited</a> <span class="text-small text-light">14TH AUGUST 2023</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=GEMxHrMFNdMwtmohXxkPvA==">Bank One
                            Limited</a> <span class="text-small text-light">20TH JULY 2023</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=K64xYSBtQkTMFLvHkxRHSA==">Axess
                            Limited</a> <span class="text-small text-light">13TH JUNE 2023</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=CO2nmgKE5y8deHlzDfgfiw==">Attitude
                            Hospitality
                            Ltd</a> <span class="text-small text-light">15TH AUGUST 2023</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=xAOOnQB3BfE9QYWzbTVqeg==">Alteo
                            Limited</a> <span class="text-small text-light">19TH MAY 2023</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=zr32bLpg3kVMc1y6EDfA1w==">MCB Group
                            Limited</a> <span class="text-small text-light">14TH AUGUST 2023</span> </div>
                </li>
            </ul>
        </div>
    </div>
</section>

<section class="reports-sec padding-100">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12 ">
                <div class="d-flex justify-content-md-between align-items-md-start flex-column flex-md-row">
                    <div>
                        <h2 class="heading-1 text-dark">Ratings</h2>
                    </div>
                    <div class="d-flex mobileChangeOuter">
                        <div class="slider-arrows me-0 mt-3 mb-3  mt-lg-0 mb-lg-0 me-2"> <span
                                class="prev-btn slick-arrow slick-disabled" aria-disabled="true" style="">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-left" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                    </path>
                                </svg>
                            </span> <span class="next-btn slick-arrow" style="" aria-disabled="false">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                    </path>
                                </svg>
                            </span> </div>
                        <div class="d-flex mobileChangeInner">
                            <div class="input-group me-2">
                                <input type="text" placeholder="From Date"
                                    class="form-control float-right hasDatepicker" id="FromDate" name="FromDate"
                                    autocomplete="off">
                            </div>
                            <div class="input-group me-2">
                                <input type="text" placeholder="To Date" class="form-control float-right hasDatepicker"
                                    id="ToDate" name="ToDate" autocomplete="off">
                            </div>
                            <div class="input-group me-2">
                                <input type="text" placeholder="Company Name" class="form-control float-right"
                                    id="cname" name="cname" autocomplete="off">
                            </div>
                            <div class="input-group">
                                <input type="button" value="Search" class="btn btn-primary form-control float-right"
                                    id="search" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row gx-5 mt-5">
            <div class="col-md-12" id="renderHtmlCompanyRelatedReportsSection">
                <div class="row reports-slider sliderGap-30">
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202404120435_Miwa_Sugar_Limited_-__Press_Release_April_2024.pdf"
                                    target="_blank" tabindex="0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=7KXW6CE7ElcRkr4ygWi0sg==" tabindex="0">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">3RD APRIL
                                        2024</span>
                                    <p class="heading-3 text-white mb-3 mt-3">Miwa Sugar limited</p>
                                </div>
                            </a> </div>
                    </div>
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202107120406_Maxcity_PR_2021.pdf" target="_blank"
                                    tabindex="0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=FymTiyNomHF/KZ/JfSMzhw==" tabindex="0">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">2ND JULY
                                        2021</span>
                                    <p class="heading-3 text-white mb-3 mt-3">MaxCity Property Fund Ltd</p>
                                </div>
                            </a> </div>
                    </div>
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202403100457_Attitude_Property_Ltd__-_Press_Release_29_March_2024.pdf"
                                    target="_blank" tabindex="0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=PZ86TN0+jCiYLvRzlR03lw==" tabindex="0">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">29TH MARCH
                                        2024</span>
                                    <p class="heading-3 text-white mb-3 mt-3">Attitude Property Ltd</p>
                                </div>
                            </a> </div>
                    </div>
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202302110339_CIM_Financial_Services_Ltd_-_Press_Release_20_February_2023.pdf"
                                    target="_blank" tabindex="0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=ZRr1eOreaL97AAuksfeFvQ==" tabindex="0">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">20TH FEBRUARY
                                        2023</span>
                                    <p class="heading-3 text-white mb-3 mt-3">CIM Financial Services Ltd</p>
                                </div>
                            </a> </div>
                    </div>
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202403120312_COVIFRA_-_Press_Release_05_March_2024.pdf"
                                    target="_blank" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=mXncS7RjGthCPRnggwxcPA==" tabindex="-1">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">5TH MARCH
                                        2024</span>
                                    <p class="heading-3 text-white mb-3 mt-3">Compagnie des Villages de Vacances de
                                        l'Isle de France Limitée</p>
                                </div>
                            </a> </div>
                    </div>
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202209100339_Manakamana-Engineering-Hydropower-Limited-Issuer-Rating-and-Bank-Facilities-Ratings-Assigned.pdf"
                                    target="_blank" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=VlD1hRqgKi5khNSyQAij2A==" tabindex="-1">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">22ND SEPTEMBER
                                        2022</span>
                                    <p class="heading-3 text-white mb-3 mt-3">Manakamana Engineering Hydropower
                                        Limited-Issuer Rating and Bank Facilities Ratings Assigned</p>
                                </div>
                            </a> </div>
                    </div>
                    <div class="item latest">
                        <div class="card card-style-1"><span class="arrow-link"><a
                                    href="upload/CompanyFiles/PR/202402100311_ENL_Limited_-_Press_Release_23_February_2024.pdf"
                                    target="_blank" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                        </g>
                                    </svg>
                                </a> </span> <a href="search?Id=Dre0BNbjrYJEWXo+KrUFHA==" tabindex="-1">
                                <div class="card-body"><span class="text-grey text-small fr_date_php">23RD FEBRUARY
                                        2024</span>
                                    <p class="heading-3 text-white mb-3 mt-3">ENL Limited</p>
                                </div>
                            </a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'components/footer.php' ?>