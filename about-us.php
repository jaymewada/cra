<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active">About Us</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">About Us</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 design-vec-bg">

    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-md-11" id="renderHtmlAboutUsOverViewSectiondata">




                <div class="row align-items-center">
                    <div class="col-md-12 col-xl-7">
                        <h3 class="heading-1 text-black ">Company Profile</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <p class="heading-3 para-line-height"></p>
                        <p style="text-align: justify;">CARE Ratings Nepal Limited (CRNL) is incorporated in Kathmandu,
                            Nepal and is the second credit rating agency to be licensed by the Securities Board of Nepal
                            w.e.f. November 16, 2017. CRNL provides credit ratings and related services in the geography
                            of Nepal.</p>
                        <p style="text-align: justify;">CARE Ratings Limited, India (CARE Ratings) is the holding
                            company of CRNL. With three decades of experience in the analytics business and a deep
                            understanding of the operations of various industries and sectors, CARE Ratings is in a
                            sound position to offer itself as a knowledge
                            hub and publishes insightful reports on the Indian and global economy, and industry research
                            papers regularly. CRNL gets technical support in the areas such as rating systems and
                            procedures, methodologies, etc. from CARE Ratings on an ongoing basis. The technical support
                            ensures that CRNL has adequate resources to provide high quality credit opinions in Nepal.
                        </p>
                        <p style="text-align: justify;">CRNL has constituted its Rating Committee with members
                            comprising officials from CARE Ratings, India. In Nepal, CRNL provides ratings for a variety
                            of industries, such as
                            manufacturing, infrastructure, and the financial sector, which includes banking, and non-
                            financial services.</p>
                        <p></p>
                    </div>
                    <div class="col-md-8 col-xl-4 offset-xl-1 mt-3 mt-xl-0">
                        <img src="https://www.careratingsnepal.com/storage/app/admin/images/about-profile_1675092727_1683269640.jpeg"
                            alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="padding-100 bg-grey-100  vision-mission-sec">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h3 class="heading-1 text-black ">Vision, Mission and Values</h3>
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                    </div>
                </div>
                <div class="row gx-5 hang-boxes Vision-mission-sldier " id="renderHtmlMissionVissionSection">
                    <div class="col-md-4">
                        <a tabindex="0">
                            <div class="card card-style-1 px-3">
                                <div class="card-body ">
                                    <p class="heading-3 text-white mb-3">Our Vision</p>
                                    <span class="text-white heading-6 height-f">
                                        <p>A Global Research &amp; Analytics company that enables risk mitigation and
                                            superior decision making</p>
                                    </span>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a tabindex="0">
                            <div class="card card-style-1 px-3">
                                <div class="card-body ">
                                    <p class="heading-3 text-white mb-3">Our Mission</p>
                                    <span class="text-white heading-6 height-f">
                                        <p>To provide best-in-class tools, analyses and insights, enabling stakeholders
                                            in Mauritian and African financial markets to make informed decisions.</p>

                                        <ul>
                                            <li>To build a pre-eminent position for ourselves in Mauritius and Africa in
                                                Risk analysis, ESG and information services</li>
                                            <li>To earn customer satisfaction and investor confidence through fairness
                                                and professional excellence</li>
                                            <li>To remain deeply committed to our internal and external stakeholders
                                            </li>
                                            <li>To apply the best possible tools &amp; techniques for risk analysis
                                                aimed to ensure efficiency and top quality</li>
                                            <li>To ensure globally comparable quality standards in our rating, research
                                                and information service&nbsp;</li>
                                            <li>&nbsp;</li>
                                        </ul>
                                    </span>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a tabindex="0">
                            <div class="card card-style-1 px-3">
                                <div class="card-body ">
                                    <p class="heading-3 text-white mb-3">Our Values</p>
                                    <span class="text-white heading-6 height-f">
                                        <ul>
                                            <li>Integrity and Transparency: Commitment to be ethical, sincere and open
                                                in our dealings</li>
                                            <li>Pursuit of Excellence: Committed to strive relentlessly to constantly
                                                improve ourselves</li>
                                            <li>Fairness: Treat clients, employees and other stakeholders fairly</li>
                                            <li>Independence: Unbiased and fearless in expressing our opinion</li>
                                            <li>Thoroughness: Rigorous analysis and research on every assignment that we
                                                take</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="boardof-deirector-sec padding-100 ">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-md-11">

                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <h3 class="heading-1 text-black">Board of Directors</h3>

                    <div class="slider-arrows me-0">
                        <span class="prev-btn">

                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/icons/arrow-left.png">
                        </span>

                        <span class="next-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/icons/arrow-right.png">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="boardof-director-slider left-space-sldier">
        <?php for ($x = 0; $x <= 10; $x++) { ?>
        <div>
            <div class="profile-card p-0">
                <img src="public/frontend-assets/images/bod.jpeg" class="img-fluid" alt="Mr. Najib Shah">
                <h4 class="pers-name">Mr. Najib Shah</h4>
                <span class="profile-name">Chairman</span>
                <a href="#" data-bs-toggle="modal" data-bs-target="#about_leaders_modal0"
                    class="btn btn-link primary d-block p-0 mt-3 text-start font-regular" tabindex="0">Read
                    more</a>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<section class="boardof-deirector-sec padding-100">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <h3 class="heading-1 text-black">Advisory Board</h3>

                    <div class="slider-arrows me-0">
                        <span class="prev-btn">

                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/icons/arrow-left.png">
                        </span>

                        <span class="next-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/icons/arrow-right.png">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="boardof-director-slider left-space-sldier">
        <?php for ($x = 0; $x <= 10; $x++) { ?>
        <div>
            <div class="profile-card p-0">
                <img src="public/frontend-assets/images/bod.jpeg" class="img-fluid" alt="Mr. Najib Shah">
                <h4 class="pers-name">Mr. Najib Shah</h4>
                <span class="profile-name">Chairman</span>
                <a href="#" data-bs-toggle="modal" data-bs-target="#about_leaders_modal0"
                    class="btn btn-link primary d-block p-0 mt-3 text-start font-regular" tabindex="0">Read
                    more</a>
            </div>
        </div>
        <?php } ?>
    </div>
</section>


<section class="senior-managment1-sec padding-100 pt-0">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <h3 class="heading-1 text-black">Senior Management</h3>

                    <div class="slider-arrows me-0">

                        <span class="prev-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets//images/icons/arrow-left.png">
                        </span>
                        <span class="next-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets//images/icons/arrow-right.png">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="senior-management-slider left-space-sldier">
        <?php for ($x = 0; $x <= 10; $x++) { ?>
        <div>
            <div class="profile-card p-0">
                <img src="public/frontend-assets/images/bod.jpeg" class="img-fluid" alt="Mr. Najib Shah">
                <h4 class="pers-name">Mr. Najib Shah</h4>
                <span class="profile-name">Chairman</span>
                <a href="#" data-bs-toggle="modal" data-bs-target="#about_leaders_modal0"
                    class="btn btn-link primary d-block p-0 mt-3 text-start font-regular" tabindex="0">Read
                    more</a>
            </div>
        </div>
        <?php } ?>
    </div>

    <div class="modal fade leadership_modal" id="exampleModal0" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close" jf-ext-button-ct="×">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <img src="https://www.careratingsafrica.com/storage/app/admin/images/image 1_1698759975.png"
                                class="img-fluid" alt="Mr. Saurav Chatterjee">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 leadership_modal_content_div">
                            <h4 class="pers-name ">Mr. Saurav Chatterjee</h4>
                            <span class="profile-name">Director &amp; CEO</span>
                            <div class="leadership_modal_p_div">
                                <p></p>
                                <p>Mr. Saurav Chatterjee is the Director &amp; CEO of CARE Ratings (Africa) Private
                                    Limited since March 2016. He has<br>
                                    been associated with CARE Ratings Limited, India. (a premier Rating Agency in
                                    India)
                                    in senior roles for major<br>
                                    part of his career with a stint in corporate banking operations of Standard
                                    Chartered Bank. He is a seasoned risk<br>
                                    professional with vast experience in Indian and African markets. His core
                                    competencies include financial<br>
                                    engineering, risk assessment and technical analysis. Mr. Chatterjee has been
                                    instrumental in setting up and<br>
                                    scaling operations for the Rating agency in Mauritius. He is well versed with
                                    statutory and legal framework for<br>
                                    various jurisdictions of African Union. A versatile and dynamic personality, he
                                    deep
                                    dives into various aspects of<br>
                                    governance and risk whilst assessing corporates. He has served as member of
                                    Rating
                                    Committee of CARE<br>
                                    Ratings for more than a decade. He is well known in the regulatory and
                                    institutional
                                    circles for having popularized<br>
                                    the concept of credit rating in the Mauritian markets. His vision is to make
                                    capital
                                    markets accessible to<br>
                                    stakeholders in African Union through disclosures, transparency and price
                                    discovery
                                    by adopting global best<br>
                                    practices. Mr. Chatterjee takes personal interest in mentoring young analysts to
                                    be
                                    risk specialists<br>
                                    and future leaders. He is also committed and passionate about making education
                                    affordable to<br>
                                    children hailing from vulnerable society. He holds a Master’s in Financial &amp;
                                    Business Administration and a fellow<br>
                                    charter from the CFA Society, India. He is keen golfer and an ambassador for
                                    Indian
                                    cultural and historical<br>
                                    aspects of art.&nbsp;</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


<?php include "components/footer.php" ?>