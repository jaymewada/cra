<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">CAPABILITIES</a></li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">STRUCTURED FINANCE</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">STRUCTURED FINANCE</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 " style="background-color: #F0F0F0; ">
    <div class="container-fluid" id="renderHtmlOverviewCapabilitySectiondata">



        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-top">
                    <div class="col-md-12 col-xl-12">
                        <h3 class="heading-1 text-black ">Overview</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <p>Structured Finance rating encompasses both retail assets and corporate assets in the African
                            context. The space covers both off-balance sheet and on-balance sheet structures. It focuses
                            on opining on specific customized solutions and largely covers all forms of facilities
                            whether extended in loan form or tradable instrument form.In the corporate assets space,
                            partial / full guarantee structures, pooled issuances have recently become prevalent. In
                            Securitization, identified pool of assets are packaged together and sold to a SPV which in
                            turn issues PTCs (Pass Through Certificates) as financial instruments to the investor. The
                            SPV is typically set up as a Trust and is managed by the trustee. In a direct assignment
                            transaction, the identified assets are sold on a bilateral basis to the investor (and no
                            separate SPV is set up). The Servicer (typically, Originator in Local jurisdiction) is
                            appointed by the SPV / investor to collect the underlying loans. The collections from the
                            underlying pool of assets are used to pay the investors. In case of a securitization
                            transaction, there is generally provision for credit enhancement to achieve target level of
                            rating for PTCs. The credit enhancement can be external (cash collateral, guarantee etc.) or
                            internal (over collateral, subordinated PTCs, subordination of Enhanced Income Securities
                            etc.) and can be provided by the Seller or a third party. The Credit Enhancement is provided
                            to an SPV to cover the losses associated with the pool of assets and can be split into first
                            loss and second loss (sequence of utilization within Credit Enhancement). There can also be
                            provision for liquidity facility in securitization to take care of temporary mismatches. In
                            direct assignment transaction, there are restrictions on Seller providing credit
                            enhancement.</p>

                        <p>In general, a company or any other entity can be Seller / Originator. However, there is
                            dominance of Banks, Non-Bank Financial Companies (NBFCs), Housing Finance Companies (HFCs)
                            as Originator / Seller. The underlying assets are mainly secured loans like housing loans,
                            auto loans, commercial vehicle loans, construction equipment loans, two-wheeler loans,
                            tractor loans, three-wheeler loans, gold loans and unsecured loans like micro finance,
                            personal loans, consumer durable loans.</p>

                        <p>CRAF uses the suffix ‘(SO)’ for securitization transactions indicating that these are
                            structured obligations.</p>
                    </div>
                    <div class="col-md-8 col-xl-5 offset-xl-1 mt-3 mt-xl-0">
                        <div class="row g-4">

                        </div>
                    </div>
                </div>
            </div>







        </div>
    </div>
</section>


<?php include "components/footer.php" ?>