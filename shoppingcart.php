<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shopping Cart</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">
                        Shopping Cart
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="payment-sum-sec">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="alert alert-success print-success-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <div class="alert alert-danger print-error-msg" style="display:none">
                                <ul></ul>
                            </div>
                        </div>
                        <h3 class="heading-2 text-black mb-5 pb-5">No Items Added to Cart</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="accordian-style-1 style-2 accordion">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <a href="https://www.careratingsafrica.com/find-ratings"
                            class="btn btn-primary ms-lg-4 make-pay float-right">Add Reports</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>