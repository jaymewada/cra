<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact us</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Contact us</h1>
            </div>
        </div>
    </div>
</section>

<section class="about-contact-sec padding-100 bg-img"
    style="background-image:url('./public/frontend-assets/images/about-contact-bg.jpg');">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-11 col-md-12">
                <div class="row gx-5">
                    <div class="col-lg-5 col-md-12">
                        <h2 class="heading-1 text-white mb-3">CARE RATINGS (AFRICA) PRIVATE LIMITED</h2>
                        <!-- <p class="heading-3 text-white">Share your requirement and<br> we will call you back in 24 hours</p> -->
                        <p class="heading-3 text-white"><b>Contact Persons:</b> </p>
                        <hr class="text-white"><br>
                        <p class="heading-5 text-white"><b>Name:</b> Saurav Chatterjee</p>
                        <p class="heading-5 text-white"><b>Mobile:</b> +230 5862 6551</p>
                        <p class="heading-5 text-white"><b>Email:</b> saurav.chatterjee@careratingsafrica.com</p>
                        <hr class="text-white"><br>
                        <p class="heading-5 text-white"><b>Name:</b> Mr. Vidhyasagar Lingesan</p>
                        <p class="heading-5 text-white"><b>Mobile:</b> +230 5273 1406</p>
                        <p class="heading-5 text-white"><b>Email:</b> vidhya.sagar@careratingsafrica.com</p>
                        <hr class="text-white"><br>


                        <p class="heading-5 text-white"><b>Address:</b> 5th Floor, MTML Square, 63, Cyber City, Ebene,
                            Mauritius</p>
                        <p class="heading-5 text-white"><b>Office Tel:</b> +230 5955 3060</p>

                    </div>
                    <div class="col-lg-7  col-md-12">
                        <div class="contact-form bg-secondary ">
                            <form class="row form-style-1" action="#">
                                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                                    <h4 class="heading-3 text-white">Business Enquiry</h4>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="email" class="form-control" placeholder="Full Name"
                                            jf-ext-cache-id="7">
                                        <label for="floatingInput">Full Name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" placeholder="1234567890"
                                            jf-ext-cache-id="8">
                                        <label for="floatingInput">Mobile Number</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating">
                                        <input type="email" class="form-control" placeholder="name@example.com"
                                            jf-ext-cache-id="9">
                                        <label for="floatingInput">Email</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating select-drop">
                                        <select name="ddlPurpose" id="ddlPurpose" class="form-select"
                                            fdprocessedid="lvt35o" jf-ext-cache-id="15">
                                            <option value="--Select Purpose--">--Select Purpose--</option>
                                            <option value="Buisness Enquiry">Buisness Enquiry</option>
                                            <option value="Recent Reports">Recent Reports</option>
                                            <option value="Get Rated">Get Rated</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating ">
                                        <input type="text" class="form-control" placeholder="Company Name"
                                            jf-ext-cache-id="10">
                                        <label for="floatingInput">Company Name</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating select-drop">
                                        <select name="ddlProduct" id="ddlProduct"
                                            class="validate-selection fortext form-select" fdprocessedid="n2urzf"
                                            jf-ext-cache-id="16">
                                            <option value="-1">--Select Interest--</option>
                                            <option value="Meenal.Sikchi@careratings.com">Ratings</option>
                                            <option value="Meenal.Sikchi@careratings.com">Gradings</option>
                                            <option value="madan.sabnavis@careratings.com">Economics</option>
                                            <option value="prashant.borole@careratings.com">SME/SSI</option>
                                            <option value="ankur.sachdeva@careratings.com">Research</option>
                                            <option value="Meenal.Sikchi@careratings.com">Customer Complaints</option>
                                            <option value="suchita.shirgaonkar@careratings.com">Others</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="text" class="form-control" placeholder="Message"
                                            jf-ext-cache-id="11">
                                        <label for="floatingInput">Message</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-floating ">
                                        <div class="google-captcha mt-5">
                                            <img src="./public/frontend-assets/images/captcha.png" width="280px">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-floating mt-5">
                                    <button type="submit" class="btn btn-primary btn-default"
                                        jf-ext-button-ct="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="public/frontend-assets/js/contact-us-map.js"></script>
<?php include "components/footer.php" ?>